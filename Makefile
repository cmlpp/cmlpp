all: 
	$(MAKE) -C lib
	$(MAKE) -C test/parser
	$(MAKE) -C test/runner
	$(MAKE) -C test/testbench

clean: 
	$(MAKE) -C lib clean
	$(MAKE) -C test/parser clean
	$(MAKE) -C test/runner clean
	$(MAKE) -C test/testbench clean

.PHONY: all clean

