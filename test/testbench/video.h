#ifndef __VIDEO_H__
#define __VIDEO_H__

#include <vector.h>

class Bullet;
class Object;
class Vector;

class VideoInterface 
{
 public:
  virtual int init( unsigned int w, unsigned int h ) = 0;

  virtual void startFrame() = 0;
  virtual void finishFrame() = 0;

  virtual void renderObject( Object* object ) = 0;
  virtual void renderBullet( Bullet* bullet ) = 0;
  virtual void renderPlayer( const Vector& vector ) = 0;
};

class Video : public VideoInterface
{
 public:

  static Video* instance();

  ~Video();

  int init( unsigned int w, unsigned int h );

  void startFrame();
  void finishFrame();

  void renderObject( Object* object );
  void renderBullet( Bullet* bullet );
  void renderPlayer( const Vector& vector );

 private:
  
  VideoInterface* _p;
};

#endif // __VIDEO_H__
