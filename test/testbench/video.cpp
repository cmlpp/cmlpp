#include <SDL.h>

#include <debug.h>
#include <object.h>
#include <bullet.h>

#include "video.h"


class VideoSDL : public VideoInterface
{
public:

  int init( unsigned int w, unsigned int h );

  void startFrame();
  void finishFrame();

  void renderObject( Object* object );
  void renderBullet( Bullet* bullet );
  void renderPlayer( const Vector& vector );

private:
  SDL_Surface* _screen;
};


int VideoSDL::init( unsigned int w, unsigned int h )
{
  _screen = SDL_SetVideoMode( w, h, 0, SDL_DOUBLEBUF | SDL_HWSURFACE );
  if( !_screen )
    return -1;

  return 0;
}

void VideoSDL::startFrame()
{
  SDL_FillRect( _screen, 0, SDL_MapRGBA( _screen->format, 0, 0, 0, 255 ) );
}

void VideoSDL::finishFrame()
{
  SDL_Flip( _screen );
}

void VideoSDL::renderObject( Object* object )
{
  float x = object->absoluteX();
  float y = object->absoluteY();

  SDL_Rect r = { (short)( x - 8.0f ), (short)( y - 8.0f ), 16, 16 };
  SDL_FillRect( _screen, &r, SDL_MapRGBA( _screen->format, 0, 0, 255, 255 ) );
}

void VideoSDL::renderBullet( Bullet* bullet )
{
  float x = bullet->absoluteX();
  float y = bullet->absoluteY();

  SDL_Rect r = { (short)( x - 4.0f ), (short)( y - 4.0f ), 8, 8 };
  SDL_FillRect( _screen, &r, SDL_MapRGBA( _screen->format, 255, 0, 0, 255 ) );
}

void VideoSDL::renderPlayer( const Vector& position )
{
  SDL_Rect r = { (short)( position.x() - 4.0f ), (short)( position.y() - 4.0f ), 8, 8 };
  SDL_FillRect( _screen, &r, SDL_MapRGBA( _screen->format, 0, 255, 0, 255 ) );
}

Video* Video::instance()
{
  static Video v;
  return &v;
}

Video::~Video()
{
  delete _p;
}

int Video::init( unsigned int w, unsigned int h )
{
  _p = new VideoSDL;
  return _p->init( w, h );
}

void Video::startFrame()
{
  _p->startFrame();
}

void Video::finishFrame()
{
  _p->finishFrame();
}

void Video::renderObject( Object* object )
{
  _p->renderObject( object );
}

void Video::renderBullet( Bullet* bullet )
{
  _p->renderBullet( bullet );
}

void Video::renderPlayer( const Vector& position )
{
  _p->renderPlayer( position );
}

