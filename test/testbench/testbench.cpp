#include <stdio.h>
#include <stdlib.h>

#include <string>

#include <SDL.h>

#include "debugtree.h"
#include "objectpool.h"
#include "parser.h"
#include "parserapi.h"
#include "program.h"
#include "scanner.h"

#include "video.h"


enum State {
  
  RunState,
  StepState,
  DoneState
};

enum Buttons {

  LeftButton,
  RightButton,
  UpButton,
  DownButton,
  NumButtons
};

State g_lastRunState = StepState;
State g_state = StepState;
Uint32 g_aTicks = 0;

void setState( State state )
{
  switch( state ) {

  case RunState:
  case StepState:
    g_lastRunState = state;
    break;
  default:
    break;
  }

  g_aTicks = 0;
  g_state = state;
}


int main( int argc, char** argv )
{
  if( argc != 2 ) {

    fprintf( stderr, "ERROR: No Cannon ML file specified\n" );
    exit( 1 );
  }
  
  FILE* fp = fopen( argv[ 1 ], "r" );
  if( !fp ) {
    
    fprintf( stderr, "ERROR: Could not open file '%s'\n", argv[ 1 ] );
    exit( 1 );
  }
      
  char buffer[ 128 * 1024 ];
  size_t r = fread( buffer, 1, sizeof( buffer ) - 1, fp );
  if( r == 0 ) {
    
    fprintf( stderr, "ERROR: Could not read file '%s'\n", argv[ 1 ] );
  }
      
  buffer[ r ] = 0;
 
  fclose( fp );
 
  void* parser = ParseAlloc( malloc );
  Program program;
  
  //FILE* log = fopen( "parser.log", "w+" );
  //ParseTrace( log, "" );

  Scanner scanner;
  if( scanner.scanBuffer( buffer, parser, &program ) ) {
	
    //program.dump( "" );

    // Terminating parser step...
    ParserToken token( 0 );
    Parse( parser, 0, &token, &program );
	
    if( !program.statementList() ) {

      fprintf( stderr, "ERROR: No statements found!\n" );
      exit( 1 );
    }
    
    fprintf( stderr, "Parsing finished\n" );
    ParseFree( parser, free );
    
    fprintf( stderr, "Initializing program\n" );
    program.setObjectPool( new ObjectPool() );
    program.setBulletPool( new BulletPool() );
    program.init();

    SDL_Init( SDL_INIT_EVERYTHING );
    atexit( SDL_Quit );

    Video::instance()->init( 600, 450 );
    
    fprintf( stderr, "Starting program\n" );
    fprintf( stderr, "Program suspended. Press 's' to step or 'c' to run\n" );

    Vector playerPosition( 300, 400 );

    bool buttons[ NumButtons ] = { false, false, false, false };

    Uint32 ticks0 = SDL_GetTicks();
    const Uint32 PERIOD = 1000 / 60;

    Uint32 fpsTicks = 0;
    Uint32 fpsCount = 0;
    
    bool run = true;
    while( run ) {

      SDL_Event e;
      while( SDL_PollEvent( &e ) ) {
	
	switch( e.type ) {
	  
	case SDL_QUIT:
	  run = false;
	  break;

        case SDL_KEYUP:

           switch( e.key.keysym.sym ) {

           case SDLK_LEFT:
             buttons[ LeftButton ] = false;
             break;
           case SDLK_RIGHT:
             buttons[ RightButton ] = false;
             break;
           case SDLK_UP:
             buttons[ UpButton ] = false;
             break;
           case SDLK_DOWN:
             buttons[ DownButton ] = false;
             break;
           default:
             break;
           }
           break;

	case SDL_KEYDOWN:
	  
	  switch( e.key.keysym.sym ) {
	    
	  case SDLK_ESCAPE:
	    run = false;
	    break;
	    
	  case SDLK_F5:
	    // Restart
	    fprintf( stderr, "Restarting\n" );
	    program.restart();
	    if( g_state == DoneState )
	      setState( g_lastRunState );
	    break;
	    
	  case SDLK_h:
	    // "Halt"
	    if( g_state == RunState ) {

	      fprintf( stderr, "Program suspended\n" );
	      setState( StepState );
	    }
	    else {

	      fprintf( stderr, "Program already suspended/finished\n" );
	    }
	    break;
	    
	  case SDLK_s:
	    // Step
	    if( g_state != StepState ) {

	      fprintf( stderr, "Cannot step while not suspended\n" );
	      break;
	    }
	    fprintf( stderr, "Stepping\n" );
               program.setPlayerPosition( playerPosition );
	    program.update();
	    break;
	    
	  case SDLK_c:
	    // Continue
	    if( g_state != StepState ) {

	      fprintf( stderr, "Cannot resume while not suspended\n" );
	      break;
	    }
	    fprintf( stderr, "Resuming program\n" );
	    setState( RunState );
	    break;

	  case SDLK_d:
	    // Dump
	    fprintf( stderr, "Dump:\n" );
	    program.dumpInterpreterState();
	    {
	      DebugTree dt( program.objectPool(), program.bulletPool() );
	      dt.dump();
	    }
	    break;

             case SDLK_LEFT:
               buttons[ LeftButton ] = true;
               break;
             case SDLK_RIGHT:
               buttons[ RightButton ] = true;
               break;
             case SDLK_UP:
               buttons[ UpButton ] = true;
               break;
             case SDLK_DOWN:
               buttons[ DownButton ] = true;
               break;

             default:
	    break;
             }
          break;
	}
      }
      
      Uint32 ticks1 = SDL_GetTicks();
      g_aTicks += ticks1 - ticks0;
      fpsTicks += ticks1 - ticks0;
      ticks0 = ticks1;
      
      int dx = 0;
      int dy = 0;
      if( buttons[ LeftButton ] )
        dx--;
      if( buttons[ RightButton ] )
        dx++;
      if( buttons[ UpButton ] )
        dy--;
      if( buttons[ DownButton ] )
        dy++;

      if( g_state == RunState ) {
	
	while( g_aTicks >= PERIOD ) {

          playerPosition += Vector( dx, dy ).normalized() * 2.0f;
          if( playerPosition.x() < 0 )
            playerPosition.setX( 0 );
          if( playerPosition.x() > 600 )
            playerPosition.setX( 600 );
          if( playerPosition.y() < 0 )
            playerPosition.setY( 0 );
          if( playerPosition.y() > 450 )
            playerPosition.setY( 450 );


          program.setPlayerPosition( playerPosition );
	  program.update();
	  g_aTicks -= PERIOD;
	}
      }

      Video::instance()->startFrame();
      
      for( unsigned int i = 0; i < program.objectPool()->numObjects(); i++ ) {
	
	Video::instance()->renderObject( program.objectPool()->object( i ) );
      }
      for( unsigned int i = 0; i < program.bulletPool()->numBullets(); i++ ) {
	
	Video::instance()->renderBullet( program.bulletPool()->bullet( i ) );
      }
      
      Video::instance()->renderPlayer( playerPosition );
      Video::instance()->finishFrame();
      
      
      fpsCount++;
      if( fpsTicks >= 1000 * 10 ) {
	
	fprintf( stderr, "FPS: %f\n", ( fpsCount * 1000.0f ) / (float)( fpsTicks ) );
	
	fpsTicks = 0;
	fpsCount = 0;
      }

      if( program.isDone() && g_state != DoneState ) {

	fprintf( stderr, "Program done\n" );
	fprintf( stderr, "Close window or press escape to quit\n" );
	fprintf( stderr, "Press F5 to restart\n" );

	setState( DoneState );
      }
    }
  }
  else {
    
    //program.dump( "" );
    fprintf( stderr, "ERROR: Parsing failed on file '%s'\n", argv[ 1 ] );
    ParseFree( parser, free );
    exit( 1 );
  }

  return 0;
}
