#include <stdio.h>
#include <stdlib.h>

#include <string>

#include "parser.h"
#include "parserapi.h"
#include "program.h"
#include "scanner.h"

int main( int argc, char** argv )
{
  bool strict = false;
  
  for( int i = 1; i < argc; i++ ) {
    
    if( *argv[i] == '-' ) {
      
      std::string option = argv[i];
      
      if( option == "-strict" || option == "--strict" ) {

	strict = true;
      }
      else {

	fprintf( stderr, "ERROR: Unknown option '%s'\n", argv[ i ] );
	exit( 1 );
      }
    }
    else {

     FILE* fp = fopen( argv[ i ], "r" );
      if( !fp ) {

	fprintf( stderr, "ERROR: Could not open file '%s'\n", argv[ i ] );
	exit( 1 );
      }
      
      char buffer[ 128 * 1024 ];
      size_t r = fread( buffer, 1, sizeof( buffer ) - 1, fp );
      if( r == 0 ) {
	
	fprintf( stderr, "ERROR: Could not read file '%s'\n", argv[ i ] );
	continue;
      }
      
      buffer[ r ] = 0;

      void* parser = ParseAlloc( malloc );
      Program program;

      FILE* log = fopen( "parser.log", "w+" );
      ParseTrace( log, "" );

      Scanner scanner;
      if( scanner.scanBuffer( buffer, parser, &program ) ) {
	
	//program.dump( "" );

	// Terminating parser step...
	ParserToken token( 0 );
	Parse( parser, 0, &token, &program );
	
	if( !program.statementList() ) {

	  fprintf( stderr, "ERROR: No statements found!\n" );
	  exit( 1 );
	}

	fprintf( stderr, "OKAY: %s\n", argv[ i ] );
	ParseFree( parser, free );
	
	program.init();
	
	while( !program.isDone() )
	  program.update();

	fprintf( stderr, "DONE: %s\n", argv[ i ] );
      }
      else {

	//program.dump( "" );
	fprintf( stderr, "ERROR: Parsing failed on file '%s'\n", argv[ i ] );
	ParseFree( parser, free );
	
	if( strict )
	  exit( 1 );
      }
    }
  }
  
  return 0;
}
