#ifndef __CMLPP_BULLETSTATEMENT_H__
#define __CMLPP_BULLETSTATEMENT_H__

#include <string>

#include "statement.h"

class Expression;
class Fiber;
class Sequence;

//! \internal
//! \brief Represents a "f" statement in CannonML
class BulletStatement : public Statement
{
 public:
  BulletStatement( const std::string& name, ExpressionList* arguments, Sequence* sequence );
  ~BulletStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  std::string _name;
  ExpressionList* _arguments;
  Sequence* _sequence;
};

#endif // __CMLPP_BULLETSTATEMENT_H__
