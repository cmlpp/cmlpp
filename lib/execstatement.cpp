#include "execstatement.h"
#include "expressionlist.h"
#include "fiber.h"
#include "fiberpool.h"
#include "object.h"
#include "objectpool.h"
#include "sequence.h"
#include "trace.h"

ExecStatement::ExecStatement( ExpressionList* arguments, Sequence* sequence )
  : Statement( Statement::Exec ),
    _arguments( arguments ), _sequence( sequence )
{
}

ExecStatement::~ExecStatement()
{
  delete _arguments;
  delete _sequence;
}
  
void ExecStatement::enter( Fiber* f )
{
}

Statement* ExecStatement::update( Fiber* f )
{
  TRACE;

  if( _sequence->isReference() ) {

    fprintf( stderr, "REFERENCE: %s\n", _sequence->reference().c_str() );
    //return next();
  }    

  Fiber* fiber = FiberPool::instance()->allocate( f->program(), _sequence->statementList() );
  fiber->setObject( f->object() );

  // FIXME

  return next();
}
