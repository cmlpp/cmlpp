#ifndef __PARSERSTATE_H__
#define __PARSERSTATE_H__

#include <map>
#include <string>

#include "bulletpool.h"
#include "objectpool.h"
#include "statement.h"
#include "statementlist.h"
#include "labellist.h"
#include "value.h"
#include "vector.h"

//! \mainpage
//!
//! libCMLPP is an interpreter library for CannonML written in C++.
//!
//! http://gitorious.org/cmlpp/pages

//! \brief Represents a loaded and executable CannonML program.
class Program {

 public:
  Program();
  ~Program();

  //! \brief Checks if there was an error during parsing.
  //!
  //! \return "true", if an error occurred during parsing, otherwise "false".
  bool isError() const;

  //! \internal
  //! \brief Sets an error message
  //!
  //! Used by the parser to specify an error that occurred during parsing.
  void setError( const std::string& error );

  //! \brief Gives the error message reported by the parser.
  //!
  //! \return The error message set by the parser, or an empty string if no error occurred.
  const std::string& error() const;

  //! \internal
  //! \brief Sets the top level statement list.
  void setStatementList( StatementList* list );

  //! \internal
  //! \brief Returns the program's top level statement list.
  StatementList* statementList();

  //! \internal
  //! \brief Sets the top level list of label definitions.
  void setLabelList( LabelList* list );

  //! \internal
  //! \brief Adds a label definition to the list of label definitions.
  void addLabelDefinition( Label* label );

  //! \brief Initializes or resets the program to an initial state.
  bool init();

  //! \brief Indicates whether the program is finished executing.
  bool isDone();

  //! \brief Runs the program for 1 tick.
  void update();

  //! \brief Returns a global variable.
  Value global( const std::string& name ) const;

  //! \brief Sets a global variable.
  void setGlobal( const std::string& name, const Value& value );

  //! \internal
  void setObjectKilledHandler( int id, StatementList* handler );

  //! \internal
  StatementList* objectKilledHandler( int id );

  //! \internal
  void setLastSequence( Sequence* sequence );
  //! \internal
  Sequence* lastSequence();

  //! \brief Sets the object pool that the program will use.
  //!
  //! This must be called before init().
  void setObjectPool( ObjectPool* pool );
  //! \brief Returns the object pool associated with the program.
  //!
  //! \see setObjectPool
  ObjectPool* objectPool();

  //! \brief Sets the bullet pool that the program will use.
  //!
  //! This must be called before init().
  void setBulletPool( BulletPool* pool );

  //! \brief Returns the bullet pool associated with the program.
  BulletPool* bulletPool();
  
  //! \internal
  void addSequenceRef( Sequence* sequence );
  //! \internal
  void resolveRequenceRefs();

  //! \internal
  LabelList* currentLabelList();
  //! \internal
  void clearCurrentLabelList();

  //! \brief Dumps information on all fibers.
  void dumpInterpreterState();

  //! \brief Restarts the program.
  void restart();

  void setPlayerPosition( const Vector& position );
  const Vector& playerPosition() const;

 private:
  std::string _error;
  StatementList* _statementList;
  std::map< std::string, Sequence* > _sequences;
  LabelList* _labelList;
  
  std::map< int, StatementList* > _objectKilledHandlers;

  std::map< std::string, Value > _globals;

  Sequence* _lastSequence;

  std::vector< Sequence* > _sequenceReferences;

  ObjectPool* _objectPool;
  Object* _object;

  BulletPool* _bulletPool;

  Vector _playerPosition;
};

#endif // __PARSERSTATE_H__
