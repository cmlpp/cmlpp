#ifndef __CMLPP_BULLET_H__
#define __CMLPP_BULLET_H__

#include "vector.h"

class Object;

//! \brief Represents a bullet object in the interpreter.
//!
//! Users can implement their own implementation of bullet objects.
//! \see BulletPoolInterface
class Bullet
{
 public:
  Bullet();
  virtual ~Bullet();
  void init( Object* object );

  void update();

  float absoluteX() { return 0; }
  float absoluteY() { return 0; }

  Object* parent();
  void setAllocated( bool value );
  bool isAllocated() const;

 private:
  Object* _parent;
  bool _allocated;

  Vector _position;
  Vector _velocity;
  Vector _acceleration;
};

#endif // __CMLPP_BULLET_H__
