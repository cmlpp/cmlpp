#ifndef __CMPLPP_LABEL_H__
#define __CMPLPP_LABEL_H__

#include <string>

class Sequence;

//! \internal
//! \brief Represents a labeled sequence.
class Label
{
 public:
  
  Label( const std::string& name, Sequence* sequence );

  const std::string& name() const;
  void setName( const std::string& name );

  Sequence* sequence();

 private:
  std::string _name;
  Sequence* _sequence;
};

#endif 
