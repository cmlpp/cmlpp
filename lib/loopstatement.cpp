#include <cassert>

#include "expression.h"
#include "fiber.h"
#include "loopstatement.h"
#include "statement.h"
#include "statementlist.h"
#include "trace.h"

/*

 = Loops =
 
 Fiber enters loop statement
 Loop statement sets up loop variable 
 Loop statement pushes itself on the return address stack
 Loop statement returns first statement of the loop body
 
 Fiber executes first and following statements from the loop body
 At end of loop body, an empty statement is returned
 
 Fiber pops loop return address from stack
 
 

 */

LoopStatement::LoopStatement( Expression* expression, StatementList* statements )
  : Statement( Statement::Loop ),
    _expression( expression ), _statements( statements )
{
  if( !_expression )
    _expression = new Expression( -1 );
}

LoopStatement::~LoopStatement()
{
  delete _expression;
  delete _statements;
}

void LoopStatement::enter( Fiber* f )
{
  if( f->currentLoop() && f->currentLoop()->loop == this ) {

    // Loop has already been initialized
    // Set return address to execute after the looped statements are done
    f->pushReturnIp( this );
    return;
  }
  
  // Set return address to execute after the looped statements are done
  f->pushReturnIp( this );
  // Set up loop variable...
  f->pushLoop( this, _expression->evaluate( f ).toInt() );
}

Statement* LoopStatement::update( Fiber* f )
{
  TRACE;

  assert( f->currentLoop()->loop == this );

  // Update loop counter
  int c = f->currentLoop()->count;

  // Already 0: Loop is done
  if( c == 0 ) {
   
    // Cleanup stack
    f->popLoop();

    return next(); // Done
  }

  if( c > 0 ) {

    // Not infinite...
    f->currentLoop()->count = c - 1;
  }

  // Return first looped statement for the fiber to execute
  return _statements->statement( 0 );
}
