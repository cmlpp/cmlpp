#include "expressionlist.h"
#include "fiber.h"
#include "fiberpool.h"
#include "object.h"
#include "objectpool.h"
#include "objectstatement.h"
#include "program.h"
#include "sequence.h"
//#include "trace.h"

ObjectStatement::ObjectStatement( const std::string& name, ExpressionList* arguments, Sequence* sequence )
  : Statement( Statement::Object ),
    _name( name ), _arguments( arguments ), _sequence( sequence )
{
}

ObjectStatement::~ObjectStatement()
{
  delete _arguments;
  delete _sequence;
}

void ObjectStatement::enter( Fiber* f )
{
}

Statement* ObjectStatement::update( Fiber* f )
{
  //TRACE;

  ::Object* object = f->program()->objectPool()->allocate();

  if( _name == "nc" ) {

    object->setParent( f->object() );
  }

  Fiber* fiber = FiberPool::instance()->allocate( f->program(), _sequence->statementList() );
  fiber->setObject( object );

  return next();
}
