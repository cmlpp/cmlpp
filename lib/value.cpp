#include <cassert>

#include "value.h"

Value::Value() 
  : _type( Null ) 
{
}

Value::Value( bool value )
{
  _type = Bool;
  _scalarValue._boolValue = value;
}

Value::Value( int value )
{
  _type = Int;
  _scalarValue._intValue = value;
}

Value::Value( float value )
{
  _type = Float;
  _scalarValue._floatValue = value;
}

Value::Value( const std::string& value )
  : _type( String ), _stringValue( value )
{
}

Value::Value( const Value& other )
  : _type( other._type ), _scalarValue( other._scalarValue ), _stringValue( other._stringValue )
{
}

Value& Value::operator=( const Value& other )
{
  _type = other._type;
  _scalarValue = other._scalarValue;
  _stringValue = other._stringValue;

  return *this;
}


bool Value::toBool() const
{
  switch( _type ) {

  case Bool:
    return _scalarValue._boolValue;
  case Int:
    return _scalarValue._intValue != 0;
  default:
    assert( false );
  }
}

float Value::toNumeric() const
{
  switch( _type ) {

  case Int:
    return _scalarValue._intValue;
  case Float:
    return _scalarValue._floatValue;
  default:
    assert( false );
  }
}

int Value::toInt() const
{
  switch( _type ) {

  case Int:
    return _scalarValue._intValue;
  case Float:
    return _scalarValue._floatValue;
  default:
    assert( false );
  }
}

std::string Value::toString() const
{
  switch( _type ) {

  case String:
    return _stringValue;

  default:
    assert( false );
  }
}

Value::Type Value::type() const
{
  return _type;
}

bool Value::isNull() const
{
  return _type == Null;
}
