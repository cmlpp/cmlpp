#ifndef __CMLPP_EXECSTATEMENT_H__
#define __CMLPP_EXECSTATEMENT_H__

#include <string>

#include "statement.h"

class ExpressionList;
class Fiber;
class Sequence;

//! \internal
//! \brief Represents an asynchronous exec statement ("@") in the interpreter.
class ExecStatement : public Statement
{
 public:
  ExecStatement( ExpressionList* arguments, Sequence* sequence );
  ~ExecStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  ExpressionList* _arguments;
  Sequence* _sequence;
};

#endif // __CMLPP_EXECSTATEMENT_H__
