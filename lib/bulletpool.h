#ifndef __CMLPP_BULLETPOOL_H__
#define __CMLPP_BULLETPOOL_H__

#include <vector>

class Bullet;

//! \brief Interface for a container for all bullet objects.
class BulletPoolInterface
{
 public:
  virtual Bullet* allocate() = 0;
  virtual void release( Bullet* bullet ) = 0;

  virtual Bullet* bullet( unsigned int index ) = 0;
  virtual unsigned int numBullets() = 0;

  virtual void clear() = 0;
};

//! \brief Default CMLPP implementation of BulletPoolInterface
class BulletPool : public BulletPoolInterface
{
 public:
  Bullet* allocate();
  void release( Bullet* bullet );

  Bullet* bullet( unsigned int index );
  unsigned int numBullets();

  void clear();

 private:
  std::vector< Bullet* > _bullets;
};

#endif // __CMLPP_BULLETPOOL_H__
