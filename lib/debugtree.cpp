#include "debugtree.h"
#include "bullet.h"
#include "bulletpool.h"
#include "object.h"
#include "objectpool.h"

DebugTreeNode::DebugTreeNode( Object* object )
  : _object( object ), _bullet( 0 )
{
}

DebugTreeNode::DebugTreeNode( Bullet* bullet )
  : _object( 0 ), _bullet( bullet )
{
}

void DebugTreeNode::addChild( DebugTreeNode* child )
{
  _children.push_back( child );
}

void DebugTreeNode::dump( const std::string& indent )
{
  if( _object ) {

    printf( "%sOBJECT 0x%X\n", indent.c_str(), (unsigned int) _object );
    if( _object->parent() && !_object->parent()->isAllocated() ) {
      
      printf( "%sParent not allocated!\n", indent.c_str() );
    }
  }

  if( _bullet ) {

    printf( "%sBULLET 0x%X\n", indent.c_str(), (unsigned int) _bullet );
    if( _bullet->parent() && !_bullet->parent()->isAllocated() ) {
      
      printf( "%sParent not allocated!\n", indent.c_str() );
    }
  }

  for( unsigned int i = 0; i < _children.size(); ++i ) {

    _children[ i ]->dump( indent + "  " );
  }
}

DebugTree::DebugTree( ObjectPoolInterface* objects, BulletPoolInterface* bullets )
{
  for( unsigned int i = 0; i < objects->numObjects(); ++i ) {

    Object* o = objects->object( i );
    if( !o->isAllocated() )
      continue;

    _objectMap[ o ] = new DebugTreeNode( o );
  }

  for( unsigned int i = 0; i < objects->numObjects(); ++i ) {

    Object* o = objects->object( i );
    if( !o->isAllocated() )
      continue;

    if( o->parent() && o->parent()->isAllocated() ) {
      
      _objectMap[ o->parent() ]->addChild( _objectMap[ o ] );
    }
  }
}

DebugTree::~DebugTree()
{
  for( std::map< Object*, DebugTreeNode* >::iterator it = _objectMap.begin();
       it != _objectMap.end(); ++it ) {

    delete (*it).second;
  }
}

void DebugTree::dump()
{
  for( std::map< Object*, DebugTreeNode* >::iterator it = _objectMap.begin();
       it != _objectMap.end(); ++it ) {

    Object* o = (*it).first;
    DebugTreeNode* n = (*it).second;

    if( !o->parent() || !o->parent()->isAllocated() ) {
      
      n->dump( "" );
    }
  }
}
