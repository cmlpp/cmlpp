#ifndef __CMLPP_EXPRESSION_H__
#define __CMLPP_EXPRESSION_H__

#include <string>

#include "fiber.h"
#include "value.h"

//! \internal
//! \brief Represents an expression in CannonML
class Expression 
{
 public:
  enum Type {

    Default,
    Integer,
    Real,
    String,
    Variable, 
    Function,
    Unary,
    Binary
  };

  Expression( int value );
  Expression( double value );
  Expression( const std::string& value );
  Expression( Type, const std::string& name, Expression* subExpression1 = 0, Expression* subExpression2 = 0 );

  Expression( const Expression& other );
  ~Expression();

  Expression& operator=( const Expression& other );

  Value evaluate( Fiber* fiber );

 private:

  Type _type;
  std::string _name;
  Expression* _subExpression1;
  Expression* _subExpression2;

  int _intValue;
  double _realValue;
  std::string _stringValue;
};

#endif // __CMLPP_EXPRESSION_H__
