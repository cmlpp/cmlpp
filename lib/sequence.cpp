#include <cassert>

#include "expressionlist.h"
#include "label.h"
#include "labellist.h"
#include "program.h"
#include "sequence.h"
#include "statementlist.h"

Sequence::Sequence( ExpressionList* arguments, StatementList* statements )
  : _arguments( arguments ), _statements( statements ), _label( 0 )
{
  fprintf( stderr, "%s\n  %.08X\n", __FUNCTION__, (unsigned) this );
}

Sequence::Sequence( Program* program, const std::string& reference )
  : _arguments( 0 ), _statements( 0 ), _reference( reference ), _label( 0 )
{
  program->addSequenceRef( this );
  fprintf( stderr, "%s\n  %.08X\n", __FUNCTION__, (unsigned) this );
}

#if 0
Sequence::Sequence( const Sequence& other )
  : ParserNode( ParserNode::SequenceNode ),
    _arguments( other._arguments ), _statements( other._statements ),  _labelList( other._labelList )
{
}
#endif

Sequence::~Sequence()
{
}

#if 0
Sequence& Sequence::operator=( const Sequence& other )
{
  if( &other == this )
    return *this;

  delete _arguments;
  delete _statements;
  delete _labelList;

  // FIXME
  _arguments = other._arguments;
  _statements = other._statements;
  _labelList = other._labelList;

  return *this;
}
#endif


bool Sequence::isReference() const
{
  return _reference.size() > 0;
}

const std::string& Sequence::reference() const
{
  return _reference;
}

StatementList* Sequence::statementList()
{
  if( _label )
    return _label->sequence()->statementList();

  return _statements;
}

void Sequence::setLabel( Label* l ) {

  assert( !_label );
  
  _label = l;
}
