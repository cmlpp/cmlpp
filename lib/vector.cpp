#include <math.h>

#include "vector.h"

float Vector::degToRad( float deg )
{
  return deg * ( PI / 180.0f );
}

float Vector::radToDeg( float rad )
{
  return rad * ( 180.0f / PI );
}

Vector::Vector( float x, float y )
  : _x( x ), _y( y )
{
}

Vector::Vector( const Vector& other )
  : _x( other._x ), _y( other._y )
{
}

Vector& Vector::operator=( const Vector& other )
{
  _x = other._x;
  _y = other._y;
  
  return *this;
}
  
Vector Vector::operator+( const Vector& other ) const
{
  return Vector( _x + other._x, _y + other._y );
}

Vector Vector::operator-( const Vector& other ) const
{
  return Vector( _x - other._x, _y - other._y );
}

Vector Vector::operator*( float s )
{
  return Vector( _x * s, _y * s );
}

Vector Vector::operator/( float s )
{
  return Vector( _x / s, _y / s );
}

Vector& Vector::operator+=( const Vector& other )
{
  _x += other._x;
  _y += other._y;

  return *this;
}

Vector Vector::normalized() const
{
  float l = length();
  if( l == 0 )
    return Vector( 0, 0 );
  return Vector( _x / l, _y / l );
}

float Vector::length() const
{
  return sqrtf( _x * _x + _y * _y );
}

float Vector::direction() const
{
  if( _x == 0 && _y == 0 )
    return 0;

  float a = atan2f( _y, _x );
  if( a < 0 )
    a += 2 * PI;

  return radToDeg( a );
}

float Vector::angle( const Vector& other ) const
{
    return ( other - *this ).direction();
}

void Vector::setX( float x )
{
  _x = x;
}

float Vector::x() const
{
  return _x;
}

void Vector::setY( float y )
{
  _y = y;
}

float Vector::y() const
{
  return _y;
}
