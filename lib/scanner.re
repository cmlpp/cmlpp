/* CannonML scanner */

#include <stdio.h>
#include <stdlib.h>

#include <string>

#include "parser.h"
#include "parserapi.h"
#include "parsertypes.h"
#include "program.h"
#include "scanner.h"

// Utility functions...

std::string getToken( const char* start, const char* end )
{
  return std::string( start, end - start );
}

void getLineAndCol( const char* start, const char* current, int* line, int* col )
{
  *line = 1;
  *col = 1;

  while( start < current ) {
    
    if( *start == '\n' ) {
      
      *line += 1;
      *col = 1;
    }
    else {
      
      *col += 1;
    }
    
    start++;
  }
}

#define PUSH_VALUE( pType, pValue ) {					\
    ParserToken* token = new ParserToken( pType, pValue );		\
    Parse( parser, pType, token, program );				\
    if( !program->isError() )						\
      continue;								\
    int line, col;							\
    getLineAndCol( buffer, tokenStart, &line, &col );			\
    fprintf( stderr, "Parser error at line %d, col %d: %s\n", line, col, program->error().c_str() ); \
    return false;							\
  }

#define PUSH( type ) PUSH_VALUE( type, std::string() )

#define TOKEN getToken( tokenStart, cursor )

bool Scanner::scanBuffer( char* buffer, void* parser, Program* program )
{
  char* cursor = buffer;

  int line = 0;
  int column = 0;
	
  char* marker = 0;
  for( ;; ) {
    
    char* tokenStart = cursor;
    
    /*!re2c
      re2c:define:YYCTYPE  = "char";
      re2c:define:YYCURSOR = cursor;
      re2c:define:YYMARKER = marker;
      re2c:yyfill:enable   = 0;

      NEWLINE = "\r"? "\n" ;
      WHITESPACE = [ \t\r\n] ;
      ANY = [^] ;

      LABELNAME = [A-Z_] [A-Z0-9_]* ;
    */
    
    /*!re2c

      "//"     			{ /* comment */ goto skipLine; }
      "/*"			{ /* comment */ goto skipComment; }
      "'"			{ /* comment */ goto scanString; }

      "[" WHITESPACE* "s?"	{ /* Start "if"-block */ PUSH( TOK_CASE ); }
      "[" WHITESPACE* "?"	{ /* Start "if"-block */ PUSH( TOK_IF ); }
      "[" 	      		{ /* Start loop  */ PUSH( TOK_STARTLOOP ); }
      "]" 			{ /* End loop */ PUSH( TOK_ENDLOOP ); }
      "{" 			{ /* Start sequence */ PUSH( TOK_LCURLY ); }
      "}" 			{ /* End sequence */ PUSH( TOK_RCURLY ); }
      ":" 			{ /* End sequence */ PUSH( TOK_COLON ); } 
      "^&" 			{ /* "Fake" call, sets "last" sequence for future reference */ PUSH_VALUE( TOK_CALL, "^" ); }
      "&" [a-z]+		{ /* User function */ PUSH_VALUE( TOK_USER_CMD, TOKEN.substr( 1 ) ); }
      "&" 			{ /* Call sequence */ PUSH( TOK_CALL ); }

      "w?" 			{ /* Wait for condition */ PUSH( TOK_WAIT_CONDITION ); }
      "w"			{ /* Wait */ PUSH( TOK_WAIT ); }
      "~"			{ /* Wait interval */ PUSH_VALUE( TOK_WAIT, "~" ); }

      "pd"			{ /* Set position relative to "h" */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "px"			{ /* Set x position */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "py"			{ /* Set y position */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "p"			{ /* Set position */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "vd"			{ /* Set velocity relative to "h" */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "vx"			{ /* Set hor. velocity */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "vy"			{ /* Set ver. velocity */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "v"			{ /* Set velocity */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "ad"			{ /* Set acceleration relative to "h" */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "ax"			{ /* Set hor. acceleration */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); } 
      "ay"			{ /* Set ver. acceleration */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }	 
      "a"			{ /* Set acceleration */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); } 
      "gp"			{ /* Set gravity */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "rc"			{ /* Set constant rotation */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "r"			{ /* Set rotation */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "ko"			{ /* Set rotation */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "i"			{ /* Set rotation */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "m"			{ /* Set mirror */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "cd"			{ /* Set direction */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "csa"			{ /* Set absolute speed */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "csr"			{ /* Set relative speed */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "css"			{ /* Set relative speed */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }

      "^@"			{ /* Set sequence reference */ PUSH( TOK_NO_EXEC ); }
      "@ko"			{ /* On object killed */ PUSH( TOK_OBJECT_KILLED_HANDLER ); }
      "@o"			{ /* New fiber on object */ PUSH( TOK_OBJECT_EXEC ); }
      "@"			{ /* New fiber */ PUSH( TOK_EXEC ); }
      "^n"			{ /* Set object reference */ PUSH_VALUE( TOK_OBJECT, "^" ); }
      "nc"			{ /* New child object */ PUSH_VALUE( TOK_OBJECT, "nc" ); }
      "n"			{ /* New object */ PUSH_VALUE( TOK_OBJECT, "n" ); }
      "^f"			{ /* Set fire reference */ PUSH_VALUE( TOK_BULLET, "^" ); }
      "fc"			{ /* New child fire */ PUSH_VALUE( TOK_BULLET, "fc" ); }
      "f"			{ /* New fire */ PUSH_VALUE( TOK_BULLET, "f" ); }

      "qx"			{ /* Set origin x */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "qy"			{ /* Set origin y */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "q"			{ /* Set origin */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "bm"			{ /* Set multi-barrage */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "bs"			{ /* Set sequential barrage */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "br"			{ /* Set random barrage */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "bv"			{ /* Set barrage velociy */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hax"			{ /* Set absolute fixed angle */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "ha"			{ /* Set absolute angle */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hox"			{ /* Set fixed angle rel. to object */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "ho"			{ /* Set angle rel. to object */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hpx"			{ /* Set fixed angle rel. to parent object */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hp"			{ /* Set angle rel. to parent object */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "htx"			{ /* Set fixed angle rel. to target object */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "ht"			{ /* Set angle rel. to target object */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hvx"			{ /* Set fixed angle by velocity */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hv"			{ /* Set angle by velocity */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "hs"			{ /* Set sequential angle */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "td"			{ /* Set default target */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "tp"			{ /* Set parent target */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "to"			{ /* Set object target */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }
      "kf"			{ /* Kill fibers */ PUSH_VALUE( TOK_BUILTIN_CMD, TOKEN ); }

      "l"			{ /* Assign rank */ PUSH( TOK_LET ); }

      ["+-*/"] "="		{ /* Assign variable */ PUSH_VALUE( TOK_ASSIGN, TOKEN ); }

      "+" { PUSH( TOK_PLUS ); }
      "-" { PUSH( TOK_MINUS ); }
      "*" { PUSH( TOK_MUL ); }
      "/" { PUSH( TOK_DIV ); }
      "(" { PUSH( TOK_LPAREN ); }
      ")" { PUSH( TOK_RPAREN ); }

      "==" { PUSH( TOK_EQ ); }
      "!=" { PUSH( TOK_NEQ ); }
      ">=" { PUSH( TOK_GTE ); }
      "<=" { PUSH( TOK_LTE ); }
      ">" { PUSH( TOK_GT ); }
      "<" { PUSH( TOK_LT ); }

      "$sin" | "$cos" | "$tan" | "$asn" | "$acs" | "$atn" | "$sqr" | "$??" | "$?"
				{ PUSH_VALUE( TOK_BUILTIN_FUNCTION, TOKEN.substr( 1 ) ); }

      "$r" | "$i" | ( "$" [0-9] ) | "$x" | "$y" | "$sx" | "$sy" | "$vx" | "$vy" | "$v" |
	"$ho" | "$td" | ( "$o" [0-9] ) | "$o" 
				{ /* Assign variable */ PUSH_VALUE( TOK_VAR, TOKEN.substr( 1 ) ); }

      "$p.r" | "$p.i" | ( "$p." [0-9] ) | "$p.x" | "$p.y" | "$p.sx" | "$p.sy" | "$p.vx" | "$p.vy" | "$p.v" |
	"$p.ho" | "$p.td" | ( "$p.o" [0-9] ) | "$p.o" 
				{ /* Assign variable */ PUSH_VALUE( TOK_VAR, TOKEN.substr( 1 ) ); }

      "$t.r" | "$t.i" | ( "$t." [0-9] ) | "$t.x" | "$t.y" | "$t.sx" | "$t.sy" | "$t.vx" | "$t.vy" | "$t.v" |
	"$t.ho" | "$t.td" | ( "$t.o" [0-9] ) | "$t.o" 
				{ /* Assign variable */ PUSH_VALUE( TOK_VAR, TOKEN.substr( 1 ) ); }
      
      [0-9]+ "." [0-9]+		{ /* Assign variable */ PUSH_VALUE( TOK_REAL_LITERAL, TOKEN ); }
      [0-9]+ 			{ /* Assign variable */ PUSH_VALUE( TOK_INT_LITERAL, TOKEN ); }

      "," 			{ /* Assign variable */ PUSH( TOK_COMMA ); }

      "#" LABELNAME		{ /* Assign variable */ PUSH_VALUE( TOK_LABEL_DEFINITION, TOKEN.substr( 1 ) ); }

      LABELNAME ( "." LABELNAME )* 
				{ PUSH_VALUE( TOK_LABEL_REFERENCE, TOKEN ); }

      [ \t\r\n]			{ /* Eat whitespace */ continue; }
      "\000"			{ /* EOF */ return true; }
      ANY			{ /* Unmatched */ getLineAndCol( buffer, tokenStart, &line, &column ); fprintf( stderr, "UNMATCHED: Line %d, Col %d: %s\n", line, column, TOKEN.c_str() ); return false; }
    */

    continue;

 skipLine:
    /*!re2c
      NEWLINE			{ continue; }
      "\000"			{ /* EOF */ return true; }
      ANY			{ goto skipLine; }
    */
    
 skipComment:
    /*!re2c
      "*/"			{ continue; }
      "\000"			{ /* EOF */ return false; }
      ANY			{ goto skipComment; }
    */

 scanString:
    /*!re2c
      "'"			{ PUSH_VALUE( TOK_STRING_LITERAL, TOKEN ); }
      "\000"			{ /* EOF */ return false; }
      ANY			{ goto scanString; }
    */
  }
  return 0;
}
