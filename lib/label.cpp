#include "label.h"
#include "sequence.h"

Label::Label( const std::string& name, Sequence* sequence )
  : _name( name ), _sequence( sequence )
{
}

const std::string& Label::name() const
{
  return _name;
}

void Label::setName( const std::string& name )
{
  _name = name;
}

Sequence* Label::sequence()
{
  return _sequence;
}
