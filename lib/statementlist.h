#ifndef __CMLPP_STATEMENTLIST_H__
#define __CMLPP_STATEMENTLIST_H__

#include <string>
#include <vector>

#include "labellist.h"

class Statement;

//! \internal
//! \brief Represents a list of statements in the interpreter.
class StatementList //: public ParserNode
{
 public:
  StatementList();
  ~StatementList();

  void append( Statement* statement );
  void append( StatementList* statementList );

  LabelList* labelDefinitions();

  Statement* statement( unsigned int index );
  Statement* takeStatement( unsigned int index );

#if 0
  //unsigned int numStatements() const;
  //Statement* statement( unsigned int index );

  class Iterator {

  public:
      Iterator( StatementList* sl ) : _list( sl ), _next( 0 ) 
      {
      }
    
      Statement* next() {
      
	if( _next >= _list->numStatements() )
	  return 0;
	
	return _list->statement( _next++ );
      }

  private:
      StatementList* _list;
      unsigned _next;
  };
#endif

 private:
  Statement* _first;
  Statement* _last;

  LabelList* _labelDefinitions;
};

#endif // __CMLPP_STATEMENTLIST_H__
