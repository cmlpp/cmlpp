#include <cassert>

#include "object.h"
#include "objectpool.h"

Object* ObjectPool::allocate()
{
  Object* o = new Object( 0 );
  o->init();
  o->setAllocated( true );

  _objects.push_back( o );
  return o;
}

void ObjectPool::release( Object* object )
{
  object->setAllocated( false );

  for( std::vector< Object* >::iterator it = _objects.begin();
       it != _objects.end(); it++ ) {

    if( *it != object )
      continue;

    delete *it;
    _objects.erase( it );
  }
}

Object* ObjectPool::object( unsigned int index )
{
  assert( index < _objects.size() );
  return _objects[ index ];
}

unsigned int ObjectPool::numObjects()
{
  return _objects.size();
}

void ObjectPool::clear()
{
  for( unsigned int i = 0; i < _objects.size(); i++ )
    delete _objects[ i ];

  _objects.clear();
}
