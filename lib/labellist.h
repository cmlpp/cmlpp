#ifndef __CMLPP_LABELLIST_H__
#define __CMLPP_LABELLIST_H__

#include <vector>
#include <string>

class Label;

//! \internal
//! \brief Used to group labels that are defined in the same "scope".
class LabelList
{
 public:
  LabelList();
  ~LabelList();

  void append( Label* label );
  void append( LabelList* labelList );

  void prependName( const std::string& name );

  unsigned int numLabels() const;
  Label* label( unsigned int index );

 private:  
  
  std::vector< Label* > _labels;
};

#endif // __CMLPP_LABELLIST_H__
