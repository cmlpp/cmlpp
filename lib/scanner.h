#ifndef __SCANNER_H__
#define __SCANNER_H__

class Program;

//! \brief Interface to the CMLPP lexer/parser tandem.
//!
//! The Scanner class implements the entry point into 
//! the lexer/parser tandem. Tokens are recognized by
//! the Scanner and directly fed into the parser, 
//! which builds the executable representation of the 
//! script in a "Program" object.
//!
//! Example:
//! \code
//! char* buffer[ 100000 ];
//! // Read CannonML file into buffer...
//! [...]
//! void* parser = ParseAlloc( malloc );
//! Program program;
//! Scanner scanner;
//! scanner.scanBuffer( buffer, parser, &program );
//! // The "terminating" parser step...
//! ParserToken token( 0 );
//! Parse( parser, 0, &token, &program );
//! ParseFree( parser, free );
//! \endcode
class Scanner
{
 public:
  
  //! \brief Scans and parses the supplied buffer into a "Program".
  //!
  //! \param buffer The script sourcecode, read into memory as a 0 terminated string.
  //! \param lemonParser The lemon generated parser structure.
  //! \param lemonState The Program that the script is read into.
  //!
  //! \return "true" on success, otherwise "false".
  bool scanBuffer( char* buffer, void* lemonParser, Program* lemonState );
};

#endif // __SCANNER_H__
