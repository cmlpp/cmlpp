#include "caseitem.h"
#include "expression.h"
#include "statementlist.h"

CaseItem::CaseItem( Expression* expression, StatementList* statements )
  : _expression( expression ), _statements( statements )
{
}

CaseItem::~CaseItem()
{
  delete _expression;
  delete _statements;
}

float CaseItem::evaluateExpression( Fiber* fiber )
{
  return _expression->evaluate( fiber ).toNumeric();
}

StatementList* CaseItem::statementList()
{
  return _statements;
}
