#ifndef __CMLPP_WAITSTATEMENT_H__
#define __CMLPP_WAITSTATEMENT_H__

#include "statement.h"

class Expression;

//! \internal
//! \brief Represents a wait statement ("w") in the interpreter.
class WaitStatement : public Statement
{
 public:

  WaitStatement( Expression*  argument, const std::string& name = std::string() );
  ~WaitStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:
  Expression* _expression;
  unsigned int _numFrames;
  std::string _name;
};
  
#endif // __CMLPP_WAITSTATEMENT_H__
