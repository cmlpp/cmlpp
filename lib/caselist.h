#ifndef __CMLPP_CASELIST_H__
#define __CMLPP_CASELIST_H__

#include <vector>

class CaseItem;

//! \internal
//! \brief Represents a list of cases in a case statement.
class CaseList
{
 public:
  ~CaseList();

  void append( CaseItem* caseItem );
  void append( CaseList* caseList );

  unsigned int numCases();
  CaseItem* caseItem( unsigned int index );

 private:  
  std::vector< CaseItem* > _caseItems;
};

#endif // __CMLPP_CASELIST_H__
