#include "bullet.h"
#include "debug.h"
#include "fiberpool.h"
#include "label.h"
#include "object.h"
#include "program.h"
#include "sequence.h"

Program::Program()
{
  _statementList = 0;
  _labelList = 0;
  _lastSequence = 0;
  _objectPool = 0;
  _object = 0;
  _bulletPool = 0;
}

Program::~Program()
{
  delete _statementList;
  delete _labelList;
}

bool Program::isError() const
{
  return _error.size() > 0;
}

void Program::setError( const std::string& error )
{
  _error = error;
}

const std::string& Program::error() const
{
  return _error;
}

void Program::setStatementList( StatementList* list )
{
  _statementList = list;
}

StatementList* Program::statementList()
{
  return _statementList;
}

void Program::setLabelList( LabelList* list )
{
  _labelList = list;
}

bool Program::init()
{
  _object = _objectPool->allocate();

  Fiber* f = FiberPool::instance()->allocate( this, _statementList );
  f->setObject( _object );

  for( unsigned int i = 0; i < _statementList->labelDefinitions()->numLabels(); i++ ) {

    fprintf( stderr, "LABEL: #%s\n", _statementList->labelDefinitions()->label( i )->name().c_str() );
  }

  resolveRequenceRefs();

  return true;
}

void Program::restart()
{
  _objectPool->clear();
  _bulletPool->clear();
  FiberPool::instance()->clear();
  
  _object = _objectPool->allocate();

  Fiber* f = FiberPool::instance()->allocate( this, _statementList );
  f->setObject( _object );
}

bool Program::isDone()
{
  return FiberPool::instance()->isDone();
}

void Program::update()
{
  FiberPool::instance()->update();

  for( unsigned int i = 0; i < _objectPool->numObjects(); i++ )
    _objectPool->object( i )->update();

  for( unsigned int i = 0; i < _bulletPool->numBullets(); i++ )
    _bulletPool->bullet( i )->update();
}

Value Program::global( const std::string& name ) const
{
  std::map< std::string, Value >::const_iterator it = _globals.find( name );
  if( it == _globals.end() )
    return Value();

  return (*it).second;
}

void Program::setGlobal( const std::string& name, const Value& value )
{
  _globals[ name ] = value;
}

void Program::setObjectKilledHandler( int id, StatementList* handler )
{
  // FIXME
  _objectKilledHandlers[ id ] = handler;
}

StatementList* Program::objectKilledHandler( int id )
{
  std::map< int, StatementList* >::iterator it = _objectKilledHandlers.find( id );
  if( it == _objectKilledHandlers.end() )
    return 0;

  return (*it).second;
}


void Program::setLastSequence( Sequence* sequence )
{
  _lastSequence = sequence;
}

Sequence* Program::lastSequence()
{
  return _lastSequence;
}

void Program::setObjectPool( ObjectPool* pool )
{
  _objectPool = pool;
}

ObjectPool* Program::objectPool()
{
  return _objectPool;
}

void Program::setBulletPool( BulletPool* pool )
{
  _bulletPool = pool;
}

BulletPool* Program::bulletPool()
{
  return _bulletPool;
}

void Program::addSequenceRef( Sequence* sequence )
{
  //NOT_IMPLEMENTED( __FUNCTION__ );
  _sequenceReferences.push_back( sequence );
}

void Program::resolveRequenceRefs()
{
  //NOT_IMPLEMENTED( __FUNCTION__ );
  // FIXME: Nested labels...

  for( unsigned int i = 0; i < _sequenceReferences.size(); i++ ) {

    Sequence* s = _sequenceReferences[ i ];
    bool found = false;

    for( unsigned int j = 0; j < _statementList->labelDefinitions()->numLabels(); j++ ) {

      Label* l = _statementList->labelDefinitions()->label( j );
      if( l->name() == s->reference() ) {

	s->setLabel( l );
	
	found = true;
	break;
      }
    }

    if( !found ) {
      
      fprintf( stderr, "Unresolved label: %s\n", s->reference().c_str() );
    }
  }
}

void Program::dumpInterpreterState()
{
  FiberPool::instance()->dump();
}


void Program::setPlayerPosition( const Vector& position )
{
  _playerPosition = position;
}

const Vector& Program::playerPosition() const
{
  return _playerPosition;
}

