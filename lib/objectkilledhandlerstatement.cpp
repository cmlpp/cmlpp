#include "expression.h"
#include "expressionlist.h"
#include "fiber.h"
#include "objectkilledhandlerstatement.h"
#include "program.h"
#include "sequence.h"

ObjectKilledHandlerStatement::ObjectKilledHandlerStatement( ExpressionList* arguments, Sequence* sequence )
  : Statement( Statement::ObjectKilledHandler ),
    _arguments( arguments ), _sequence( sequence )
{
}

ObjectKilledHandlerStatement::~ObjectKilledHandlerStatement()
{
  delete _arguments;
  delete _sequence;
}

void ObjectKilledHandlerStatement::enter( Fiber* f )
{
}

Statement* ObjectKilledHandlerStatement::update( Fiber* f )
{
  f->program()->setObjectKilledHandler( _arguments->expression( 0 )->evaluate( f ).toInt(),
					    _sequence->statementList() );
  return next();
}
