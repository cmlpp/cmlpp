#include "debug.h"
#include "object.h"

#include <math.h>

Object::Object( Object* parent )
  : _parent( parent )
{
  _allocated = false;
}

Object::~Object()
{
}

void Object::init()
{
  _parent = 0;

  _motionType = ConstantMotion;
  _interval = 0;

  _position = Vector();
  _velocity = Vector();
  _acceleration = Vector();
  _diffAcceleration = Vector();
}

void Object::setParent( Object* parent )
{
  _parent = parent;
}

Object* Object::parent()
{
  return _parent;
}

const Vector& Object::position() const
{
  return _position;
}

void Object::setPosition( const Vector& value, int interval )
{
  _interval = interval;

  if( interval == 0 ) {
    
    // TODO: Gravity???
    if( _motionType == GravityMotion ) {
    }

    _position = value;
    _motionType = ConstantMotion;
    return;
  }

  double t = 1.0 / (double) interval;
  Vector dp = value - _position;
  _acceleration = ( dp * t * 3.0 - _velocity * 2.0 ) * t * 2.0;
  _diffAcceleration = ( dp * t * -2.0 + _velocity ) * t * t * 6.0;
  _motionType = InterpolationMotion;
}

Vector Object::absolutePosition() const
{
  if( _parent )
    return _parent->position() + _position;

  return _position;
}

const Vector& Object::velocity() const
{
  return _velocity;
}

void Object::setVelocity( const Vector& value )
{
  _velocity = value;
}

const Vector& Object::acceleration() const
{
  return _acceleration;
}

void Object::setAcceleration( const Vector& value )
{
  _acceleration = value;
}

void Object::setGravity( double gravity, double friction, int interval )
{
  if ( parent() ) 
    return;

  if( fabs( gravity ) < 0.01 && fabs( friction ) < 0.01 ) {

    // Reset gravity
    _acceleration = Vector();
    _diffAcceleration = Vector();
    _interval = 0;
    _motionType = ConstantMotion;
  }
  else {

    _diffAcceleration = Vector( gravity * 0.01, friction * 0.01 );
    _interval = interval;
    // TODO: rx/ry
    _motionType = GravityMotion;
  }
}

float Object::x() const
{
  return _position.x();
}

float Object::absoluteX() const
{
  if( _parent )
    return _parent->absoluteX() + _position.x();

  return _position.x();
}

float Object::y() const
{
  return _position.y();
}

float Object::absoluteY() const
{
  if( _parent )
    return _parent->absoluteY() + _position.y();

  return _position.y();
}

void Object::update()
{
  switch( _motionType ) {

  case ConstantMotion:
    {
      _position += _velocity;
      
      // TODO: Rotation
    }
    break;
 
  case AccelerationMotion:
    {
      _position += _velocity + _acceleration * 0.5;
      _velocity += _acceleration;
      
      // TODO: Rotation
      
      if( --_interval == 0 )
	_motionType = ConstantMotion;
    }
    break;
   
  case InterpolationMotion:
    {
      _position += _velocity + _acceleration * 0.5 + _diffAcceleration * ( 1.0 / 6.0 );
      _velocity += _acceleration + _diffAcceleration * 0.5;
      _acceleration += _diffAcceleration;
      
      // TODO: Rotation
      
      if( --_interval <= 0 ) 
	_motionType = ConstantMotion;
    }
    break;

  case GravityMotion:
    {
      Vector dir = position() - absolutePosition();
      
      _acceleration = Vector( dir.x()  * _diffAcceleration.x() - _velocity.x() * _diffAcceleration.y(),
			      dir.y()  * _diffAcceleration.x() - _velocity.y() * _diffAcceleration.y() );
      _position += _velocity + _acceleration * 0.5;
      _velocity += _acceleration;
      
      // TODO: Rotation
      
      if( --_interval <= 0 ) 
	_motionType = ConstantMotion;
    }
    break;

  case BulletMLMotion:
    NOT_IMPLEMENTED( "BulletMLMotion" );
    break;
  }
}

void Object::setAllocated( bool value )
{
  _allocated = value;
}

bool Object::isAllocated() const
{
  return _allocated;
}

void Object::setTarget( TargetType type, const Vector& position )
{
  _targetType = type;
  _targetPosition = position;
}
