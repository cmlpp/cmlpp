#include "callstatement.h"
#include "fiber.h"
#include "sequence.h"
#include "statementlist.h"
#include "trace.h"

CallStatement::CallStatement( Sequence* sequence )
  : Statement( Call ),
    _sequence( sequence )
{
}

CallStatement::~CallStatement()
{
  delete _sequence;
}
  
void CallStatement::enter( Fiber* f )
{
  f->pushReturnIp( next() );
}

Statement* CallStatement::update( Fiber* f )
{
  TRACE;
  return _sequence->statementList()->statement( 0 );
}
