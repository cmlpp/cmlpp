#ifndef __CMLPP_EXPRESSIONLIST_H__
#define __CMLPP_EXPRESSIONLIST_H__

#include <vector>

class Expression;

//! \internal
//! \brief Represents a list of expressions in the interpreter.
class ExpressionList
{
 public:
  ExpressionList();
  ~ExpressionList();

  void append( Expression* e );
  void append( ExpressionList* list );

  unsigned int numExpressions() const;
  Expression* expression( unsigned int index );

 private:
  std::vector< Expression* > _expressions;
};

#endif // __CMLPP_EXPRESSIONLIST_H__
