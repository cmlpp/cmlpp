#ifndef __CMLPP_SWITCHSTATEMENT_H__
#define __CMLPP_SWITCHSTATEMENT_H__

#include "statement.h"

class CaseList;
class Expression;
class Fiber;
class StatementList;

//! \internal
//! \brief Represents a switch statement in the interpreter.
class SwitchStatement : public Statement
{
 public:
  SwitchStatement( Expression* expression, StatementList* switchStatements, CaseList* caseList );
  ~SwitchStatement();

  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:
  Expression* _expression;
  StatementList* _statements;
  CaseList* _caseList;
};

#endif // __CMLPP_SWITCHSTATEMENT_H__
