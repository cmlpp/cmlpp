#include <cassert>

#include "caselist.h"
#include "expression.h"
#include "expressionlist.h"
#include "sequence.h"
#include "statement.h"
#include "statementlist.h"

#include "builtincmdstatement.h"
#include "usercmdstatement.h"
#include "waitstatement.h"
#include "ifstatement.h"
#include "switchstatement.h"
#include "loopstatement.h"
#include "assignstatement.h"
#include "callstatement.h"
#include "execstatement.h"
#include "objectstatement.h"
#include "bulletstatement.h"
#include "objectkilledhandlerstatement.h"
#include "setlastsequencestatement.h"
#include "objectexecstatement.h"

Statement* Statement::createAssign( const std::string& name, const std::string& op, Expression* expression )
{
  return new AssignStatement( name, op, expression );
}

Statement* Statement::createIf( Expression* expression, StatementList* ifStatements, StatementList* elseStatements )
{
  return new IfStatement( expression, ifStatements, elseStatements );
}

Statement* Statement::createCase( Expression* expression, StatementList* defaultStatements, CaseList* caseItems )
{
  return new SwitchStatement( expression, defaultStatements, caseItems );
}

Statement* Statement::createLoop( Expression* expression, StatementList* statements )
{
  return new LoopStatement( expression, statements );
}

Statement* Statement::createBuiltinCmd( const std::string& name, ExpressionList* arguments )
{
  return new BuiltinCmdStatement( name, arguments );
}

Statement* Statement::createUserCmd( const std::string& name, ExpressionList* arguments )
{
  return new UserCmdStatement( name, arguments );
}

Statement* Statement::createCall( Sequence* sequence )
{
  return new CallStatement( sequence );
}

Statement* Statement::createExec( ExpressionList* arguments, Sequence* sequence )
{
  return new ExecStatement( arguments, sequence );
}

Statement* Statement::createObjectKilledHandler( ExpressionList* arguments, Sequence* sequence )
{
  return new ObjectKilledHandlerStatement( arguments, sequence );
}

Statement* Statement::createSetLastSequence( ExpressionList* arguments, Sequence* sequence )
{
  return new SetLastSequenceStatement( arguments, sequence );
}

Statement* Statement::createObjectExec( ExpressionList* arguments, Sequence* sequence )
{
  return new ObjectExecStatement( arguments, sequence );
}

Statement* Statement::createObject( const std::string& name, ExpressionList* arguments, Sequence* sequence )
{
  return new ObjectStatement( name, arguments, sequence );
}

Statement* Statement::createBullet( const std::string& name, ExpressionList* arguments, Sequence* sequence )
{
  return new BulletStatement( name, arguments, sequence );
}

Statement* Statement::createWait( Expression* argument, const std::string& name )
{
  return new WaitStatement( argument, name );
}

Statement* Statement::createWaitCondition( Expression* argument )
{
  return new WaitStatement( argument );
}

Statement::Statement( Type type )
  : /* ParserNode( ParserNode::StatementNode ), */
    _type( type ), _next( 0 )
{
  //fprintf( stderr, "Statement\n  %.08X\n", (unsigned) this );
}

Statement::~Statement()
{
  //fprintf( stderr, "~Statement\n   %.08X\n", (unsigned) this );
}

Statement::Type Statement::type() const
{
  return _type;
}

void Statement::enter( Fiber* f )
{
}

#if 0
Statement* Statement::update( Fiber* f )
{
  return 0;
}

bool Statement::isBlocking()
{
  return false;
}
#endif

#if 0
void Statement::dump( const std::string& indent )
{
  fprintf( stderr, "%sStatement ( type == %d )\n", indent.c_str(), _type );
}
#endif


Statement* Statement::next()
{
  return _next;
}

void Statement::setNext( Statement* s )
{
  _next = s;
}

