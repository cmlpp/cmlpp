#include <cassert>

#include "expression.h"
#include "expressionlist.h"
#include "parsertypes.h"

ParserToken::ParserToken( int code, const std::string& value )
  : _code( code ), _value( value )
{
}

int ParserToken::code() const
{
  return _code;
}

const std::string& ParserToken::value() const
{
  return _value;
}
