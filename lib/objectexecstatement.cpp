#include "expression.h"
#include "expressionlist.h"
#include "fiber.h"
#include "fiberpool.h"
#include "object.h"
#include "objectexecstatement.h"
#include "program.h"
#include "sequence.h"

ObjectExecStatement::ObjectExecStatement( ExpressionList* arguments, Sequence* sequence )
  : Statement( Statement::ObjectExec ),
    _arguments( arguments ), _sequence( sequence )
{
}

ObjectExecStatement::~ObjectExecStatement()
{
  delete _arguments;
  delete _sequence;
}

void ObjectExecStatement::enter( Fiber* f )
{
}

Statement* ObjectExecStatement::update( Fiber* f )
{
#if 0
  Fiber* fbr = FiberPool::instance()->allocate( f->program(), _sequence->statementList() );

  Object* o = f->program()->object( _arguments->expression( 0 )->evaluate().toInt() );
  assert( o );

  fbr->setObject( o );
#endif
  // FIXME
  return next();
}
