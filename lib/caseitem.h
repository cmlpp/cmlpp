#ifndef __CMLCPP_CASEITEM_H__
#define __CMLCPP_CASEITEM_H__

class Expression;
class Fiber;
class StatementList;

//! \internal
//! \brief Represents an item in the case list of a case statement.
class CaseItem {
  
 public:
  CaseItem( Expression* expression, StatementList* statements );
  ~CaseItem();

  float evaluateExpression( Fiber* fiber );
  StatementList* statementList();
  
 private:
  Expression* _expression;
  StatementList* _statements;
};

#endif // __CMLCPP_CASEITEM_H__
