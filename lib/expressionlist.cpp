#include <cassert>

#include "expression.h"
#include "expressionlist.h"

ExpressionList::ExpressionList()
{
}

ExpressionList::~ExpressionList()
{
  while( _expressions.size() ) {

    delete _expressions.back();
    _expressions.pop_back();
  }
}

void ExpressionList::append( Expression* e )
{
  assert( e );

  _expressions.push_back( e );
}

void ExpressionList::append( ExpressionList* list )
{
  assert( list );

  _expressions.insert( _expressions.end(), list->_expressions.begin(), list->_expressions.end() );
  list->_expressions.clear();
}


unsigned int ExpressionList::numExpressions() const
{
  return _expressions.size();
}

Expression* ExpressionList::expression( unsigned int index )
{
  assert( index < _expressions.size() );
  return _expressions[ index ];
}
