#include "expression.h"
#include "fiber.h"
#include "waitconditionstatement.h"
#include "trace.h"

WaitConditionStatement::WaitConditionStatement( Expression*  argument )
  : Statement( WaitCondition ), _expression( argument )
{
}

WaitConditionStatement::~WaitConditionStatement()
{
  delete _expression;
}
  
void WaitConditionStatement::enter( Fiber* f )
{
}

Statement* WaitConditionStatement::update( Fiber* f )
{
  TRACE;
  
  if( _expression->evaluate( f ).toBool() )
    return this;

  return next();
}
