#include <math.h>
#include <stdlib.h>

#include <cassert>

#include "expression.h"

Expression::Expression( int value )
{
  _type = Integer;
  _intValue = value;

  fprintf( stderr, "%s\n  %.08X\n", __FUNCTION__, (unsigned) this );
  fprintf( stderr, "  INT\n" );
}

Expression::Expression( double value )
{
  _type = Real;
  _realValue = value;

  fprintf( stderr, "%s\n  %.08X\n", __FUNCTION__, (unsigned) this );
  fprintf( stderr, "  REAL\n" );
}

Expression::Expression( const std::string& value )
{
  _type = String;
  _stringValue = value;

  fprintf( stderr, "%s\n  %.08X\n", __FUNCTION__, (unsigned) this );
  fprintf( stderr, "  STRING\n" );
}

Expression::Expression( Type type, const std::string& name, Expression* subExpression1, Expression* subExpression2 )
{
  _type = type;
  _name = name;
  _subExpression1 = subExpression1;
  _subExpression2 = subExpression2;

  fprintf( stderr, "%s\n  %.08X\n", __FUNCTION__, (unsigned) this );
  fprintf( stderr, "  EXPR %d %s\n", type, name.c_str() );
}

Expression::Expression( const Expression& other )
{
  _type = other._type;
  _name = other._name;
  
  if( other._subExpression1 )
    _subExpression1 = new Expression( *other._subExpression1 );
  else 
    _subExpression1 = 0;

  if( other._subExpression2 )
    _subExpression2 = new Expression( *other._subExpression2 );
  else 
    _subExpression2 = 0;

  _intValue = other._intValue;
  _realValue = other._realValue;
}

Expression::~Expression()
{
  delete _subExpression1;
  delete _subExpression2;
}

Expression& Expression::operator=( const Expression& other )
{
  if( &other == this )
    return *this;

  delete _subExpression1;
  delete _subExpression2;

  _type = other._type;
  _name = other._name;
  
  if( other._subExpression1 )
    _subExpression1 = new Expression( *other._subExpression1 );
  else 
    _subExpression1 = 0;

  if( other._subExpression2 )
    _subExpression2 = new Expression( *other._subExpression2 );
  else 
    _subExpression2 = 0;

  _intValue = other._intValue;
  _realValue = other._realValue;

  return *this;
}

Value Expression::evaluate( Fiber* fiber )
{
  switch( _type ) {

  case Default:
    break;

  case Integer:
    return Value( _intValue );

  case Real:
    return Value( (float) _realValue );

  case String:
    return Value( _stringValue );

  case Variable:
    return fiber->global( _name );

  case Function:
    
    if( _name == "sin" ) {

      return Value( sinf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "cos" ) {

      return Value( cosf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "tan" ) {
      
      return Value( tanf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "asn" ) {
      
      return Value( asinf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "acs" ) {
      
      return Value( acosf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "acs" ) {
      
      return Value( acosf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "sqr" ) {
      
      return Value( sqrtf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "int" ) {
      
      return Value( (int) floorf( _subExpression1->evaluate( fiber ).toNumeric() ) );
    }
    else if( _name == "??" ) {
      
      return Value( 1.0f - ( (float) rand() ) / ( (float) RAND_MAX / 2.0f ) );
    }
    else if( _name == "?" ) {
      
      return Value( ( (float) rand() ) / ( (float) RAND_MAX ) );
    }
    break;

  case Unary:
    if( _name == "-" ) {

      return Value( 0 - _subExpression1->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "!" ) {

      return Value( !_subExpression1->evaluate( fiber ).toBool() );
    }
    break;
    
  case Binary:
    if( _name == "+" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() + _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "-" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() - _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "*" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() * _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "/" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() / _subExpression2->evaluate( fiber ).toNumeric() );
    }

    else if( _name == "==" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() == _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "!=" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() != _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == ">" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() > _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == ">=" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() >= _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "<" ) {
      
      return Value( _subExpression1->evaluate( fiber ).toNumeric() < _subExpression2->evaluate( fiber ).toNumeric() );
    }
    else if( _name == "<=" ) {

      return Value( _subExpression1->evaluate( fiber ).toNumeric() <= _subExpression2->evaluate( fiber ).toNumeric() );
    }
    break;
  }

  return Value(); // Null
}
