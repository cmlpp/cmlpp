#ifndef __VALUE_H__
#define __VALUE_H__

#include <string>

//! \brief Generic datatype.
class Value {

 public:

  enum Type {

    Null,
    Bool,
    Int,
    Float,
    String,
  };

  Value();
  Value( bool value );
  Value( int value );
  Value( float value );
  Value( const std::string& value );
  Value( const Value& other );

  Value& operator=( const Value& other );

  bool toBool() const;
  float toNumeric() const;
  int toInt() const;
  std::string toString() const;

  Type type() const;
  bool isNull() const;

 private:
  Type _type;
  
  union {

    bool _boolValue;
    int _intValue;
    float _floatValue;

  } _scalarValue;

  std::string _stringValue;
};


#endif // __VALUE_H__
