#ifndef __CMLPP_USERCMDSTATEMENT_H__
#define __CMLPP_USERCMDSTATEMENT_H__

#include <string>

#include "statement.h"

class ExpressionList;
class Fiber;

//! \internal
//! \brief Represents a call to a user defined function in the interpreter.
class UserCmdStatement : public Statement
{
 public:
  UserCmdStatement( const std::string& name, ExpressionList* arguments );
  ~UserCmdStatement();

  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:
  std::string _name;
  ExpressionList* _arguments;
};

#endif // __CMLPP_USERCMDSTATEMENT_H__
