#ifndef __CMLPP_STATEMENT_H__
#define __CMLPP_STATEMENT_H__

#include <string>

class Expression;
class ExpressionList;
class CaseList;
class Fiber;
class Sequence;
class StatementList;

//! \internal
//! \brief Base class for the representation of CannonML statements in the interpreter.
class Statement /*: public ParserNode*/
{
 public:

  enum Type {

    Assign,
    If,
    Case,
    Loop,
    BuiltinCmd,
    UserCmd,
    Call,
    Exec,
    ObjectKilledHandler,
    SetLastSequence,
    ObjectExec,
    Object,
    Bullet,
    Wait,
    WaitCondition
  };

  static Statement* createAssign( const std::string& name, const std::string& op, Expression* expression );
  static Statement* createIf( Expression* expression, StatementList* ifStatements, StatementList* elseStatements );
  static Statement* createCase( Expression* expression, StatementList* defaultStatements, CaseList* caseList );
  static Statement* createLoop( Expression* expression, StatementList* statements );
  static Statement* createBuiltinCmd( const std::string& name, ExpressionList* arguments );
  static Statement* createUserCmd( const std::string& name, ExpressionList* arguments );
  static Statement* createCall( Sequence* sequence );
  static Statement* createExec( ExpressionList* arguments, Sequence* sequence );
  static Statement* createObjectKilledHandler( ExpressionList* arguments, Sequence* sequence );
  static Statement* createSetLastSequence( ExpressionList* arguments, Sequence* sequence );
  static Statement* createObjectExec( ExpressionList* arguments, Sequence* sequence );

  static Statement* createObject( const std::string& name, ExpressionList* arguments, Sequence* sequence );
  static Statement* createBullet( const std::string& name, ExpressionList* arguments, Sequence* sequence );
  static Statement* createWait( Expression* argument, const std::string& name = std::string() );
  static Statement* createWaitCondition( Expression* argument );


  Statement( Type type );
  ~Statement();

  Type type() const;

  //void dump( const std::string& );

  virtual void enter( Fiber* f ) = 0;
  virtual Statement* update( Fiber* f ) = 0;

  void setNext( Statement* s );
  Statement* next();

 private:
  Type _type;
  Statement* _next;
};

#endif // __CMLPP_STATEMENT_H__
