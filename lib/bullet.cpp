#include "bullet.h"

Bullet::Bullet()
  : _parent( 0 ), _allocated( false )
{
}

Bullet::~Bullet()
{
}

void Bullet::init( Object* object )
{
}

void Bullet::update()
{
}

Object* Bullet::parent()
{
  return _parent;
}

void Bullet::setAllocated( bool value )
{
  _allocated = value;
}

bool Bullet::isAllocated() const
{
  return _allocated;
}

