#ifndef __CMLPP_OBJECTSTATEMENT_H__
#define __CMLPP_OBJECTSTATEMENT_H__

#include <string>

#include "statement.h"

class Expression;
class Fiber;
class Sequence;

//! \internal
//! \brief Represents an object creating statement ("o") in the interpreter.
class ObjectStatement : public Statement
{
 public:
  ObjectStatement( const std::string& name, ExpressionList* arguments, Sequence* sequence );
  ~ObjectStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  std::string _name;
  ExpressionList* _arguments;
  Sequence* _sequence;
};

#endif // __CMLPP_OBJECTSTATEMENT_H__
