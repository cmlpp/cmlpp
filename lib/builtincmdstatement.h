#ifndef __CMLPP_BUILTINCMDSTATEMENT_H__
#define __CMLPP_BUILTINCMDSTATEMENT_H__

#include <string>

#include "statement.h"

class ExpressionList;
class Fiber;

//! \internal
//! \brief Represents a built-in command in the interpreter.
class BuiltinCmdStatement : public Statement
{
 public:
  BuiltinCmdStatement( const std::string& name, ExpressionList* arguments );
  ~BuiltinCmdStatement();

  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:
  std::string _name;
  ExpressionList* _arguments;
};

#endif // __CMLPP_BUILTINCMDSTATEMENT_H__
