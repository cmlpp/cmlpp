#include "fiberpool.h"
#include "trace.h"

FiberPool* FiberPool::instance()
{
  static FiberPool fp; 
  return &fp;
}

Fiber* FiberPool::allocate( Program* ps, StatementList* sl )
{
  TRACE;

  _fibers.push_back( new Fiber( ps, sl ) );
  return _fibers.back();
}

void FiberPool::release( Fiber* f )
{
  for( std::vector< Fiber* >::iterator it = _fibers.begin();
       it != _fibers.end(); it++ ) {
    
    if( *it == f ) {

      _fibers.erase( it );
      break;
    }
  }

  delete f;
}


void FiberPool::update()
{
  for( unsigned int i = 0; i < _fibers.size(); i++ )
    _fibers[ i ]->update();
}

bool FiberPool::isDone()
{
  for( unsigned int i = 0; i < _fibers.size(); i++ )
    if( !_fibers[ i ]->isDone() )
      return false;

  return true;
}

void FiberPool::dump()
{
  fprintf( stderr, "================\n" );
  fprintf( stderr, "Fibers: (%d total)\n", _fibers.size() );
  fprintf( stderr, "----------------\n" );
  for( unsigned int i = 0; i < _fibers.size(); i++ ) {


    _fibers[ i ]->dump();
  }

  fprintf( stderr, "\n" );
}

void FiberPool::clear()
{
  for( unsigned int i = 0; i < _fibers.size(); i++ )
    delete _fibers[ i ];

  _fibers.clear();
}
