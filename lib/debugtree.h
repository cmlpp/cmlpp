#ifndef __DEBUGTREE_H__
#define __DEBUGTREE_H__

#include <map>
#include <string>
#include <vector>

class Object;
class ObjectPoolInterface;
class Bullet;
class BulletPoolInterface;

//! \brief Represents a node in the object hierarchy tree.
class DebugTreeNode
{
 public:
  DebugTreeNode( Object* object );
  DebugTreeNode( Bullet* bullet );

  void addChild( DebugTreeNode* child );
  void dump( const std::string& indent );

 private:
  Object* _object;
  Bullet* _bullet;
  std::vector< DebugTreeNode* > _children;
};

//! \brief Represents the object hierarchy tree.
//!
//! Utility class for debugging. A hierarchy tree of all
//! currently allocated objects can be created and dumped.
class DebugTree
{
 public:
  DebugTree( ObjectPoolInterface* objects, BulletPoolInterface* bullets );
  ~DebugTree();

  void dump();

 private:
  std::map< Object*, DebugTreeNode* > _objectMap;
};

#endif // __DEBUGTREE_H__
