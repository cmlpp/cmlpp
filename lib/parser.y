%include { #include <stdlib.h> }
%include { #include <cassert> }

%include { #include "program.h" }
%include { #include "parsertypes.h" }

%token_type { ParserToken* }
%token_prefix TOK_

%extra_argument { Program* program }

/* Precedence rules */
%left PLUS MINUS .
%left MUL DIV .
%nonassoc EQ NEQ GTE GT LTE LT .
%right NOT .

%syntax_error {

  program->setError( "PARSER ERROR: SYNTAX ERROR" );
}

%stack_overflow {

  program->setError( "PARSER ERROR: STACK OVERFLOW" );
}

%default_type { void* }

%type statementlist { StatementList* }
%type statement { Statement* }
%type expression { Expression* }
%type expression_or_empty { Expression* }
%type bexpression { Expression* }
%type expressionlist { ExpressionList* }
%type constantlist { ExpressionList* }
%type constant { Expression* }
%type caselist { CaseList* }
%type case { CaseItem* }
%type sequence { Sequence* }
%type sequence_or_labelref { Sequence* }
%type labeldef { Label* }


/* %type expressionlist { ParserExpressionList } */
/* %type expression { ParserExpression } */
/* %type expression_or_empty { ParseExpression } */

/*
%type expressionlist { TokenType* }
%type expression { TokenType* }
%type expression_or_empty { TokenType* }
%type bexpression { TokenType* }
*/

/* Start symbol of grammar 
   Every Cannon ML program consists of a list of statements
*/
start ::= statementlist(A) . 
{
  // Top-level list of statements 
  program->setStatementList( A );
} 

/* List of statements, cannot be empty */
statementlist(A) ::= statementlist(B) statement(C) .
{
  A = new StatementList;
  A->append( B );
  A->append( C );

  //delete B;
} 

statementlist(A) ::= statementlist(B) labeldef(C) .
{
  A = new StatementList;
  A->append( B );
  A->labelDefinitions()->append( C );

  //delete B;
} 

statementlist(A) ::= statement(B) .
{
  A = new StatementList;
  A->append( B );
} 

statementlist(A) ::= labeldef(B) .
{
  A = new StatementList;
  A->labelDefinitions()->append( B );
} 

/* Statements */

/* Variable assignment */
statement(A) ::= LET VAR(B) ASSIGN(C) expression(D) .
{
  //A = 0;
  A = Statement::createAssign( B->value(), C->value(), D );
}

/* "IF" statement */
statement(A) ::= IF bexpression(B) statementlist(C) ENDLOOP .
{
  //A = 0;
  A = Statement::createIf( B, C, 0 );
}

/* "IF : ELSE" statement */
statement(A) ::= IF bexpression(B) statementlist(C) COLON statementlist(D) ENDLOOP .
{
  //A = 0;
  A = Statement::createIf( B, C, D );
}

/* "CASE"  statement */
statement(A) ::= CASE expression(B) statementlist(C) caselist(D) ENDLOOP .
{
  //A = 0;
  A = Statement::createCase( B, C, D );
}
  
caselist(A) ::= caselist(B) case(C) .
{
  //A = 0;
  A = new CaseList;
  A->append( B );
  A->append( C );

  //delete B;
}

caselist(A) ::= case(B) .
{
  //A = 0;
  A = new CaseList;
  A->append( B );
}

case(A) ::= COLON expression(B) statementlist(C) .
{
  //A = 0;
  A = new CaseItem( B, C );
}

/* Loop with count argument */
statement(A) ::= STARTLOOP expression(B) statementlist(C) ENDLOOP .
{
  //A = 0;
  A = Statement::createLoop( B, C );
}


/* Endless loop */
statement(A) ::= STARTLOOP statementlist(C) ENDLOOP .
{
  //A = 0;
  A = Statement::createLoop( 0, C );
}

/* Call of builtin command */
statement(A) ::= BUILTIN_CMD(B) expressionlist(C) .
{
  //A = 0;
  A = Statement::createBuiltinCmd( B->value(), C );
}

/* Call of user command */
statement(A) ::= USER_CMD(B) expressionlist(C) .
{
  //A = 0;
  A = Statement::createUserCmd( B->value(), C );
}

/* "Free" sequence */
/* Problematic... not supported atm */
/* statement ::= sequence_or_labelref . */

/* Synchronous call of sequence ("&") */
statement(A) ::= CALL sequence_or_labelref(B) .
{
  //A = 0;
  A = Statement::createCall( B );
}
  
/* Asynchronous call of sequence ("@", new fiber) */
statement(A) ::= EXEC expressionlist(C) sequence_or_labelref(D) .
{
  //A = 0;
  A = Statement::createExec( C, D );
}

/* @ko ... */
statement(A) ::= OBJECT_KILLED_HANDLER expressionlist(C) sequence_or_labelref(D) .
{
  A = Statement::createObjectKilledHandler( C, D );
}

/* ^@ ... */
statement(A) ::= NO_EXEC expressionlist(C) sequence_or_labelref(D) .
{
  A = Statement::createSetLastSequence( C, D );
}

/* ^@ ... */
statement(A) ::= OBJECT_EXEC expressionlist(C) sequence_or_labelref(D) .
{
  A = Statement::createObjectExec( C, D );
}

/* Object creation with attached sequence ("n", "nc") */
statement(A) ::= OBJECT(B) expressionlist(C) sequence_or_labelref(D) .
{
  //A = 0;
  A = Statement::createObject( B->value(), C, D );
}

/* Bullet creation with attached sequence ("f{...}", "fc{ ... }") */
statement(A) ::= BULLET(B) expressionlist(C) sequence_or_labelref(D) .
{
  //A = 0;
  A = Statement::createBullet( B->value(), C, D );
}

/* Bullet creation with implicit sequence ("f", "fc") */
statement(A) ::= BULLET(B) expressionlist(C) .
{
  //A = 0;
  A = Statement::createBullet( B->value(), C, 0 );
}

/* Label definition ( "#LABEL{...}") */
labeldef(A) ::= LABEL_DEFINITION(B) sequence(C) .
{
  //A = 0;
  A = new Label( B->value(), C );
}

/* Wait statements */

statement(A) ::= WAIT expression(B) .
{
  //A = 0;
  A = Statement::createWait( B );
}

statement(A) ::= WAIT(B) .
{
  //A = 0;
  A = Statement::createWait( 0, B->value() );
}

statement(A) ::= WAIT_CONDITION  bexpression(B) .
{
  //A = 0;
  A = Statement::createWaitCondition( B );
}

/* Expressions */

/* Arithmetic expressions */
expression(A) ::= expression(B) PLUS(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

expression(A) ::= expression(B) MINUS(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

expression(A) ::= expression(B) MUL(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

expression(A) ::= expression(B) DIV(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

expression(A) ::= MINUS(B) expression(C) . [NOT] 
{ 
  //A = 0;
  A = new Expression( Expression::Unary, B->value(), C );
}

/* Constant expressions */
expression(A) ::= INT_LITERAL(B) . 
{ 
  //A = 0;
  A = new Expression( atoi( B->value().c_str() ) );
}

expression(A) ::= REAL_LITERAL(B) . 
{ 
  //A = 0;
  A = new Expression( atof( B->value().c_str() ) );
}

/* Variable reference  */
expression(A) ::= VAR(B) . 
{ 
  //A = 0;
  A = new Expression( Expression::Variable, B->value() );
}

/* Function call */
expression(A) ::= BUILTIN_FUNCTION(B) expression(C) . [NOT] 
{ 
  //A = 0;
  A = new Expression( Expression::Function, B->value(), C );
}

/* Parentheses */
expression(A) ::= LPAREN expression(B) RPAREN . 
{ 
  //A = 0;
  A = B;
}

/* Comma-separated of expressions, can be empty or contain empty parts */
expressionlist(A) ::= expressionlist(B) COMMA expression_or_empty(C) . 
{ 
  //A = 0;
  A = new ExpressionList;
  A->append( B );
  A->append( C );

  delete B;
}

expressionlist(A) ::= expression_or_empty(B) . 
{ 
  //A = 0;
  A = new ExpressionList;
  A->append( B );
}

expression_or_empty(A) ::= expression(B) . 
{ 
  //A = 0;
  A = B;
}

expression_or_empty(A) ::= . 
{ 
  //A = 0;
  A = new Expression( Expression::Default, "" );
}

/* Logic expressions */
bexpression(A) ::= expression(B) EQ(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

bexpression(A) ::= expression(B) NEQ(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

bexpression(A) ::= expression(B) GT(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D );
}

bexpression(A) ::= expression(B) GTE(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D ); 
}

bexpression(A) ::= expression(B) LT(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D ); 
}
  
bexpression(A) ::= expression(B) LTE(C) expression(D) . 
{ 
  //A = 0;
  A = new Expression( Expression::Binary, C->value(), B, D ); 
}

bexpression(A) ::= NOT(B) bexpression(C) . 
{ 
  //A = 0;
  A = new Expression( Expression::Unary, B->value(), C ); 
}

bexpression(A) ::= LPAREN bexpression(B) RPAREN . 
{ 
  //A = 0;
  A = B; 
}

/* Sequences */
sequence_or_labelref(A) ::= sequence(B) .
{
  //A = 0;
  A = B;
}

sequence_or_labelref(A) ::= LABEL_REFERENCE(B) .
{
  //A = 0;
  A = new Sequence( program, B->value() );
}

/* Sequence body */
sequence(A) ::= LCURLY constantlist(B) statementlist(C) RCURLY .
{
  //A = 0;
  A = new Sequence( B, C );
}

sequence(A) ::= LCURLY constantlist(B) RCURLY .
{
  //A = 0;
  A = new Sequence( B, 0 );
}

/* Reference to last sequence */
sequence(A) ::= LCURLY DOT RCURLY .
{
  //A = 0;
  A = new Sequence( program, "." );
}

/* Comma seperated list of constants, can be empty or contain empty parts */
constantlist(A) ::= constant(B) COMMA constantlist(C) .
{
  //A = 0;
  A = new ExpressionList;
  A->append( B );
  A->append( C );

  delete C;
}

constantlist(A) ::= constant(B) .
{
  //A = 0;
  A = new ExpressionList;
  A->append( B );
}

constantlist(A) ::= .
{
  //A = 0;
  A = new ExpressionList;
  A->append( new Expression( Expression::Default ) );
}

/* Constants */
constant(A) ::= INT_LITERAL(B) .
{
  A = 0;
  A = new Expression( atoi( B->value().c_str() ) );
}

constant(A) ::= REAL_LITERAL(B) .
{
  A = 0;
  A = new Expression( atof( B->value().c_str() ) );
}
  
constant(A) ::= STRING_LITERAL(B) .
{
  A = 0;
  A = new Expression( B->value() );
}
