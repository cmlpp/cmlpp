#include <cassert>

#include "fiber.h"
#include "program.h"
#include "statement.h"
#include "statementlist.h"

Fiber::Fiber( Program* ps, StatementList* slist )
  : _program( ps ), _statements( slist ), _index( 0 ), _loopStackPtr( -1 ), _done( false ), _object( 0 )
{
  _ip = slist->statement( 0 );
  assert( _ip );

  _ip->enter( this );

  _interval = 0;
}

bool Fiber::isDone() const
{
  return _done;
}

void Fiber::update()
{
  if( _done )
    return;

  assert( _ip );

  for( ;; ) {
    
    Statement* nextIp = _ip->update( this );
    if( nextIp == _ip ) {

      // Blocking
      return;
    }

    _ip = nextIp;

    while( !_ip && _stack.size() ) {

      _ip = _stack.back();
      _stack.pop_back();
    }

    if( !_ip ) {

      _done = true;
      return;
    }
    
    _ip->enter( this );
  }
}

unsigned int Fiber::lastWaitFrames()
{
  return _lastWaitFrames;
}

unsigned int Fiber::currentWaitFrames()
{
  assert( _waitFrames.size() > 0 );

  return _waitFrames.back();
}

void Fiber::setCurrentWaitFrames( unsigned int frames)
{
  assert( _waitFrames.size() > 0 );

  _waitFrames.back() = frames;
}

void Fiber::pushWaitFrames( unsigned int frames )
{
  _waitFrames.push_back( frames );
  _lastWaitFrames = frames;
}

unsigned int Fiber::popWaitFrames()
{
  assert( _waitFrames.size() > 0 );
  
  unsigned int f = _waitFrames.back();
  _waitFrames.pop_back();
  return f;
}

Fiber::LoopStackEntry* Fiber::currentLoop()
{
  if( !_loopStack.size() )
    return 0;

  return &_loopStack[ _loopStackPtr ];
}

void Fiber::setCurrentLoop( LoopStatement* loop, int value )
{
  assert( _loopStack.size() );

  _loopStack[ _loopStackPtr ].loop = loop;
  _loopStack[ _loopStackPtr ].count = value;
}

void Fiber::pushLoop() {

  assert( !loopStackIsTop() );
  
  ++_loopStackPtr;
}

void Fiber::pushLoop( LoopStatement* loop, int count )
{
  if( loopStackIsTop() ) {
    
    _loopStack.push_back( LoopStackEntry() );
    _loopStack.back().loop = loop;
    _loopStack.back().count = count;
    _loopStackPtr++;
  }
  else {

    _loopStackPtr++;
    _loopStack[ _loopStackPtr ].loop = loop;
    _loopStack[ _loopStackPtr ].count = count;
  }
}

void Fiber::popLoop()
{
  assert( _loopStackPtr > 0 );

  _loopStackPtr--;
}
 
bool Fiber::loopStackIsTop()
{
  return _loopStackPtr == ( (int) _loopStack.size() ) - 1;
}
 
void Fiber::pushReturnIp( Statement* statement )
{
  _stack.push_back( statement );
}

void Fiber::setGlobal( const std::string& name, const Value& value )
{
  _program->setGlobal( name, value );
}

Value Fiber::global( const std::string& name ) const
{
  return _program->global( name );
}

Program* Fiber::program()
{
  return _program;
}

Object* Fiber::object()
{
  return _object;
}

void Fiber::setObject( Object* o )
{
  assert( !_object );

  _object = o;
}

int Fiber::interval() const
{
  return _interval;
}

void Fiber::setInterval( int value )
{
  _interval = value;
}

bool Fiber::hasInterval() const
{
  return _interval > 0;
}

void Fiber::dump()
{
  fprintf( stderr, "Fiber: 0x%X\n", (unsigned) this );
  fprintf( stderr, "  %s\n", _done ? "done" : "active" );
  fprintf( stderr, "  ip: 0x%X %d\n", (unsigned) _ip, _ip ? _ip->type() : -1 );
  fprintf( stderr, "  object: 0x%X\n", (unsigned) _object );
}
