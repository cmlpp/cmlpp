#ifndef __CMLPP_PARSERTYPES_H__
#define __CMLPP_PARSERTYPES_H__

#include "caseitem.h"
#include "caselist.h"
#include "expression.h"
#include "expressionlist.h"
#include "label.h"
#include "labellist.h"
#include "sequence.h"
#include "statement.h"
#include "statementlist.h"

//! \brief ParserToken objects are created by the lexer and passed to the parser.
class ParserToken 
{
public:

  //! \brief Constructor
  //! \param code The numeric token identifier. The constants for these are automatially created by re2c in the file parser.h.
  //! \param value The actual string value of the token.
  ParserToken( int code, const std::string& value = "" );

  //! \brief The numeric token id.
  int code() const;
  //! \param value The actual string value of the token.
  const std::string& value() const;

private:
  int _code;
  std::string _value;
};

#endif // __CMLPP_PARSERTYPES_H__
