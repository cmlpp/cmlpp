#include "bullet.h"
#include "bulletstatement.h"
#include "expressionlist.h"
#include "fiber.h"
#include "program.h"
#include "sequence.h"
#include "trace.h"

BulletStatement::BulletStatement( const std::string& name, ExpressionList* arguments, Sequence* sequence )
  : Statement( Statement::Bullet ),
    _name( name ), _arguments( arguments ), _sequence( sequence )
{
}

BulletStatement::~BulletStatement()
{
  delete _arguments;
  delete _sequence;
}

void BulletStatement::enter( Fiber* f )
{
}

Statement* BulletStatement::update( Fiber* f )
{
  ::Bullet* bullet = f->program()->bulletPool()->allocate();
  return next();
}
