#ifndef __CMLPP_OBJECTPOOL_H__
#define __CMLPP_OBJECTPOOL_H__

#include <vector>

class Object;

//! \brief Interface for the object pool.
//!
//! The object pool is used by the interpreter to
//! allocate and destroy objects.
//! Programs using libCMLPP can provide their own
//! implementation of this interface, or use the
//! default implementation ObjectPool.
//!
//! \see Program::setObjectPool
class ObjectPoolInterface
{
 public:
  virtual Object* allocate() = 0;
  virtual void release( Object* object ) = 0;

  virtual Object* object( unsigned int index ) = 0;
  virtual unsigned int numObjects() = 0;

  virtual void clear() = 0;
};

//! \brief Default implementation of the object pool interface.
class ObjectPool : public ObjectPoolInterface
{
 public:
  Object* allocate();
  void release( Object* object );

  Object* object( unsigned int index );
  unsigned int numObjects();

  void clear();

 private:
  std::vector< Object* > _objects;
};

#endif // __CMLPP_OBJECTPOOL_H__
