#include <cassert>

#include "label.h"
#include "labellist.h"
#include "statement.h"
#include "statementlist.h"

StatementList::StatementList()
//  : ParserNode( ParserNode::StatementListNode )
  : _first( 0 ), _last( 0 )
{
  //_labelDefinitions = 0;
  _labelDefinitions = new LabelList();
  //_labelDefinitions->setParent( this );

  //fprintf( stderr, "StatementList\n  %.08X\n", (unsigned) this );
}

StatementList::~StatementList()
{
  delete _labelDefinitions;
  delete _first;

  //delete _labelDefinitions;
  //fprintf( stderr, "~StatementList\n  %.08X\n", (unsigned) this );
}

void StatementList::append( Statement* statement )
{
  assert( statement );
  
  //if( !statement ) 
  //  return;

  if( !_first ) {
    
    _first = statement;
    _last = statement;

    // FFWD to the last statement
    while( _last->next() )
      _last = _last->next();

    return;
  }
  
  _last->setNext( statement );
  _last = statement;
  
  // FFWD to the last statement
  while( _last->next() )
    _last = _last->next();
}

void StatementList::append( StatementList* list )
{
  assert( list );
  append( list->takeStatement( 0 ) );
}

LabelList* StatementList::labelDefinitions()
{
  return _labelDefinitions;
}

#if 0
unsigned int StatementList::numStatements() const
{ 
  return 
    _statements.size(); 
}

Statement* StatementList::statement( unsigned int index ) 
{ 
  assert( index < _statements.size() ); 
  return _statements[ i ]; 
}
#endif


Statement* StatementList::statement( unsigned int index )
{
  Statement* s = _first;
  while( s && index ) {

    s = s->next();
    index--;
  }

  return s;
}

Statement* StatementList::takeStatement( unsigned int index )
{
  Statement* s = _first;
  Statement* t = 0;
  while( s && index ) {

    t = s;
    s = s->next();
    index--;
  }

  if( s == _first ) {

    _first = 0;
    _last = 0;

    return s;
  }

  _last = t;
  _last->setNext( 0 );

  return s;
}
