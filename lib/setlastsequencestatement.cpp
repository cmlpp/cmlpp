#include "expression.h"
#include "expressionlist.h"
#include "fiber.h"
#include "program.h"
#include "sequence.h"
#include "setlastsequencestatement.h"

SetLastSequenceStatement::SetLastSequenceStatement( ExpressionList* arguments, Sequence* sequence )
  : Statement( Statement::SetLastSequence ),
    _arguments( arguments ), _sequence( sequence )
{
}

SetLastSequenceStatement::~SetLastSequenceStatement()
{
  delete _arguments;
  delete _sequence;
}

void SetLastSequenceStatement::enter( Fiber* f )
{
}

Statement* SetLastSequenceStatement::update( Fiber* f )
{
  f->program()->setLastSequence( _sequence );
  return next();
}
