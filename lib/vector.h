#ifndef __CMLPP_VECTOR_H__
#define __CMLPP_VECTOR_H__

//! \brief 2D vector type.
class Vector
{
 public:

  static const float PI = 3.14;
  static float degToRad( float deg );
  static float radToDeg( float rad );

  Vector( float x = 0, float y = 0);
  Vector( const Vector& other );
  
  Vector& operator=( const Vector& other );
  
  Vector operator+( const Vector& other ) const;
  Vector operator-( const Vector& other ) const;

  Vector operator*( float s );
  Vector operator/( float s );

  Vector& operator+=( const Vector& other );

  Vector normalized() const;
  
  float length() const;

  float direction() const;

  float angle( const Vector& other ) const;

  void setX( float x );
  float x() const;

  void setY( float y );
  float y() const;

 private:
  float _x;
  float _y;
};

#endif // __CMLPP_VECTOR_H__
