#ifndef __CMLPP_FIBERPOOL_H__
#define __CMLPP_FIBERPOOL_H__

#include <vector>

#include "fiber.h"

class Program;
class StatementList;

//! \internal
//! \brief Pool for allocation of fibers (microthreads).
class FiberPool
{
 public:
  
  //! \brief Singleton instance.
  static FiberPool* instance();

  Fiber* allocate( Program* ps, StatementList* sl );
  void release( Fiber* f );

  //! \brief Runs all allocated fibers for 1 tick.
  //!
  //! \see Fiber::update()
  //! \see Program::update()
  void update();

  //! \brief Indicates whether all fibers have finished executing.
  //!
  //! \see Fiber::isDone()
  //! \see Program::isDone()
  bool isDone();

  //! \brief Dumps status info on all allocated fibers.
  void dump();

  //! \brief Destroys all allocated fibers.
  void clear();

 private:
  std::vector< Fiber* > _fibers;
};

#endif // __CMLPP_FIBERPOOL_H__
