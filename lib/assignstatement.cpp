#include <cassert>

#include "assignstatement.h"
#include "expression.h"
#include "fiber.h"
#include "trace.h"

AssignStatement::AssignStatement( const std::string& name, const std::string& op, Expression* expression )
  : Statement( Statement::Assign ),
    _name( name ), _op( op ), _expression( expression )
{
}

AssignStatement::~AssignStatement()
{
  delete _expression;
}

void AssignStatement::enter( Fiber* f )
{
}

Statement* AssignStatement::update( Fiber* f )
{
  TRACE;

  Value oldValue = f->global( _name );
  if( oldValue.isNull() )
    oldValue = Value( 0.0f );

  if( _op == "=" ) {

    f->setGlobal( _name, _expression->evaluate( f ) );
  }
  else if( _op == "+=" ) {

    f->setGlobal( _name, oldValue.toNumeric() + _expression->evaluate( f ).toNumeric() );
  }
  else if( _op == "-=" ) {

    f->setGlobal( _name, oldValue.toNumeric() - _expression->evaluate( f ).toNumeric() );
  }
  else if( _op == "*=" ) {

    f->setGlobal( _name, oldValue.toNumeric() * _expression->evaluate( f ).toNumeric() );
  }
  else if( _op == "/=" ) {

    f->setGlobal( _name, oldValue.toNumeric() / _expression->evaluate( f ).toNumeric() );
  }
  else {
    
    assert( false );
  }

  return next();
}
