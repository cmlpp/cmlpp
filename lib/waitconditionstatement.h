#ifndef __CMLPP_WAITCONDITIONSTATEMENT_H__
#define __CMLPP_WAITCONDITIONSTATEMENT_H__

#include "statement.h"

class Expression;

//! \internal
//! \brief Represents a "wait on condition" statement ("w?") in the interpreter.
class WaitConditionStatement : public Statement
{
 public:

  WaitConditionStatement( Expression* argument );
  ~WaitConditionStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:
  Expression* _expression;
  unsigned int _numFrames;
};
  
#endif // __CMLPP_WAITCONDITIONSTATEMENT_H__


