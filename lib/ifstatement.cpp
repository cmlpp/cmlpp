#include "expression.h"
#include "fiber.h"
#include "ifstatement.h"
#include "statementlist.h"
#include "trace.h"

IfStatement::IfStatement( Expression* expression, StatementList* ifStatements, StatementList* elseStatements ) 
  : Statement( Statement::If ),
    _expression( expression ), _ifStatements( ifStatements ), _elseStatements( elseStatements )
{
}

IfStatement::~IfStatement()
{
  delete _expression;
  delete _ifStatements;
  delete _elseStatements;
}

void IfStatement::enter( Fiber* f )
{
}

Statement* IfStatement::update( Fiber* f )
{
  TRACE;

  if( _expression->evaluate( f ).toBool() ) {
    
    return _ifStatements->statement( 0 );
  }
  else {
    
    if( !_elseStatements )
      return 0;
    
    return _elseStatements->statement( 0 );
  }
  
  return 0;
}
