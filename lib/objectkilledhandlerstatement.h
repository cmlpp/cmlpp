#ifndef __CMLPP_OBJECTKILLEDHANDLERSTATEMENT_H__
#define __CMLPP_OBJECTKILLEDHANDLERSTATEMENT_H__

#include <string>

#include "statement.h"

class ExpressionList;
class Fiber;
class Sequence;

//! \internal
//! \brief Represents an "object killed handler" ("@ko").
class ObjectKilledHandlerStatement : public Statement
{
 public:
  ObjectKilledHandlerStatement( ExpressionList* arguments, Sequence* sequence );
  ~ObjectKilledHandlerStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  ExpressionList* _arguments;
  Sequence* _sequence;
};

#endif // __CMLPP_OBJECTKILLEDHANDLERSTATEMENT_H__
