#ifndef __CMLPP_CALLSTATEMENT_H__
#define __CMLPP_CALLSTATEMENT_H__

#include <string>

#include "statement.h"

class Expression;
class Fiber;
class Sequence;

//! \internal
//! \brief Represents a call statement ("&") in the interpreter.
class CallStatement : public Statement
{
 public:
  CallStatement( Sequence* sequence );
  ~CallStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  Sequence* _sequence;
};

#endif // __CMLPP_CALLSTATEMENT_H__
