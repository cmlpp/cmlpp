#ifndef __CMLPP_IFSTATEMENT_H__
#define __CMLPP_IFSTATEMENT_H__

#include "statement.h"

class Expression;
class Fiber;
class LabelList;
class StatementList;

//! \internal
//! \brief Represents an if-statement in the interpreter.
class IfStatement : public Statement
{
 public:
  IfStatement( Expression* expression, StatementList* ifStatements, StatementList* elseStatements );
  ~IfStatement();

  void enter( Fiber* f );
  Statement* update( Fiber* f );

  LabelList* labelList();

 private:
  Expression* _expression;
  StatementList* _ifStatements;
  StatementList* _elseStatements;
};

#endif // __CMLPP_IFSTATEMENT_H__
