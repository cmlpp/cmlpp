#ifndef __CMLPP_OBJECTEXECSTATEMENT_H__
#define __CMLPP_OBJECTEXECSTATEMENT_H__

#include <string>

#include "statement.h"

class ExpressionList;
class Fiber;
class Sequence;


//! \internal
//! \brief Represents a "execute on object" statement ("@o") in the interpreter.
class ObjectExecStatement : public Statement
{
 public:
  ObjectExecStatement( ExpressionList* arguments, Sequence* sequence );
  ~ObjectExecStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  ExpressionList* _arguments;
  Sequence* _sequence;
};

#endif // __CMLPP_OBJECTEXECSTATEMENT_H__
