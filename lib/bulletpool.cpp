#include <cassert>

#include "bullet.h"
#include "bulletpool.h"

Bullet* BulletPool::allocate()
{
  _bullets.push_back( new Bullet() );
  return _bullets.back();
}

void BulletPool::release( Bullet* bullet )
{
  for( std::vector< Bullet* >::iterator it = _bullets.begin();
       it != _bullets.end(); it++ ) {

    if( *it != bullet )
      continue;

    delete *it;
    _bullets.erase( it );
  }
}

Bullet* BulletPool::bullet( unsigned int index )
{
  assert( index < _bullets.size() );
  return _bullets[ index ];
}

unsigned int BulletPool::numBullets()
{
  return _bullets.size();
}

void BulletPool::clear()
{
  for( unsigned int i = 0; i < _bullets.size(); i++ )
    delete _bullets[ i ];

  _bullets.clear();
}
