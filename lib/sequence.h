#ifndef __CMLPP_SEQUENCE_H__
#define __CMLPP_SEQUENCE_H__

#include <string>

class ExpressionList;
class Label;
class LabelList;
class Program;
class StatementList;

//! \internal
//! \brief Represents a sequence ("{...}") in the interpreter.
class Sequence
{
 public:
  Sequence( ExpressionList* arguments, StatementList* statements );
  Sequence( Program* program, const std::string& reference );
  //Sequence( const Sequence& other );
  ~Sequence();

  //Sequence& operator=( const Sequence& other );

  bool isReference() const;
  const std::string& reference() const;

  //void setLabelList( LabelList* list );
  //LabelList* labelList( LabelList* list );

  StatementList* statementList();

  void setLabel( Label* l );

 private:
  ExpressionList* _arguments;
  StatementList* _statements;
  std::string _reference;
  Label* _label;
};

#endif // __CMLPP_SEQUENCE_H__
