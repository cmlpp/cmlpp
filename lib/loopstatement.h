#ifndef __CMLPP_LOOPSTATEMENT_H__
#define __CMLPP_LOOPSTATEMENT_H__

#include "statement.h"

class Expression;
class Fiber;
class StatementList;

//! \internal
//! \brief Represents a loop statement ("[...]") in the interpreter.
class LoopStatement : public Statement
{
 public:
  LoopStatement( Expression* expression, StatementList* statements );
  ~LoopStatement();

  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:
  Expression* _expression;
  StatementList* _statements;
};

#endif // __CMLPP_LOOPSTATEMENT_H__
