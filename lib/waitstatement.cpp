#include "expression.h"
#include "fiber.h"
#include "waitstatement.h"
#include "trace.h"

WaitStatement::WaitStatement( Expression*  argument, const std::string& name  )
  : Statement( Wait ), _expression( argument ), _name( name )
{
}

WaitStatement::~WaitStatement()
{
  delete _expression;
}
  
void WaitStatement::enter( Fiber* f )
{
  if( !_expression ) {

    if( _name == "~" ) {

      // "~" 
      f->pushWaitFrames( f->interval() );
    }
    else {

      // "w" without argument: Wait as long as previous wait statement
      f->pushWaitFrames( f->lastWaitFrames() );
    }
  }
  else {

    // "w" with argument: Wait as long as specified
    f->pushWaitFrames( _expression->evaluate( f ).toInt() );
  }
}

Statement* WaitStatement::update( Fiber* f )
{
  //TRACE;
  
  unsigned int waitFrames = f->currentWaitFrames();
  if( waitFrames > 0 ) {
    
    f->setCurrentWaitFrames( waitFrames - 1 );
    return this;
  }

  f->popWaitFrames();
  return next();
}
