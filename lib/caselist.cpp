#include <cassert>

#include "caseitem.h"
#include "caselist.h"

CaseList::~CaseList()
{
  while( _caseItems.size() ) {

    delete _caseItems.back();
    _caseItems.pop_back();
  }
}

void CaseList::append( CaseItem* caseItem )
{
  assert( caseItem );
  _caseItems.push_back( caseItem );
}

void CaseList::append( CaseList* list )
{
  assert( list );
  _caseItems.insert( _caseItems.end(), list->_caseItems.begin(), list->_caseItems.end() );
  list->_caseItems.clear();
}

unsigned int CaseList::numCases()
{
  return _caseItems.size();
}

CaseItem* CaseList::caseItem( unsigned int index )
{
  assert( index < _caseItems.size() );
  return _caseItems[ index ];
}
