#ifndef __CMLPP_DEBUG_H__
#define __CMLPP_DEBUG_H__

#include <stdio.h>

//! \internal
#define NOT_IMPLEMENTED( x ) { fprintf( stderr, "NOT IMPLEMENTED %s:%d %s\n", __FILE__, __LINE__, x ); }

#endif // __CMLPP_DEBUG_H__
