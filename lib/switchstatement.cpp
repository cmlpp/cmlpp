#include "caselist.h"
#include "caseitem.h"
#include "expression.h"
#include "fiber.h"
#include "statement.h"
#include "statementlist.h"
#include "switchstatement.h"

SwitchStatement::SwitchStatement( Expression* expression, StatementList* statements, CaseList* caseList )
  : Statement( Statement::Case ),
    _expression( expression ), _statements( statements ), _caseList( caseList )
{
}

SwitchStatement::~SwitchStatement()
{
  delete _expression;
  delete _statements;
  delete _caseList;
}

void SwitchStatement::enter( Fiber* f )
{
}

Statement* SwitchStatement::update( Fiber* f )
{
  float value = _expression->evaluate( f ).toNumeric();
  
  StatementList* list = _statements;

  for( unsigned int i = 0; i < _caseList->numCases(); i++ ) {

    if( _caseList->caseItem( i )->evaluateExpression( f ) > value ) {

      list = _caseList->caseItem( i )->statementList();
    }
    else break;
  }

  return list->statement( 0 );
}

