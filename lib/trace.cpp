#include <stdio.h>

#include "trace.h"

std::string Tracer::_indent = "";

Tracer::Tracer( const std::string& file, int line, const std::string& function )
  : _file( file ), _line( line ), _function( function )
{
  fprintf( stderr, "%s>>> %s:%d %s\n", _indent.c_str(), _file.c_str(), _line, _function.c_str() );
  _indent += "  ";
}

Tracer::~Tracer()
{
  _indent = std::string( _indent, 0, _indent.size() - 2 );
  fprintf( stderr, "%s<<< %s:%d %s\n", _indent.c_str(), _file.c_str(), _line, _function.c_str() );
}
