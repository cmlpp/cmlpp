#include <cassert>

#include "builtincmdstatement.h"

#include "debug.h"
#include "expression.h"
#include "expressionlist.h"
#include "fiber.h"
#include "object.h"
#include "program.h"
#include "trace.h"

BuiltinCmdStatement::BuiltinCmdStatement( const std::string& name, ExpressionList* arguments )
  : Statement( Statement::BuiltinCmd ), _name( name ), _arguments( arguments )
{
}

BuiltinCmdStatement::~BuiltinCmdStatement()
{
  delete _arguments;
}

void BuiltinCmdStatement::enter( Fiber* f )
{
}

Statement* BuiltinCmdStatement::update( Fiber* f )
{
  TRACE;

  assert( f->object() );
  
  Value v0 = Value();
  Value v1 = Value();
  
  if( _arguments->numExpressions() > 0 ) {
    
    v0 = _arguments->expression( 0 )->evaluate( f );
  }
  if( _arguments->numExpressions() > 1 ) {
    
    v1 = _arguments->expression( 1 )->evaluate( f );
  }
  
  if( v0.isNull() )
    v0 = Value( 0.0f );
  if( v1.isNull() )
    v1 = Value( 0.0f );
  
  if( _name == "p" ) {
    
    f->object()->setPosition( Vector( v0.toNumeric(), v1.toNumeric() ), 
			      f->interval()
			      );
  }
  else if( _name == "px" ) {
    
    f->object()->setPosition( Vector( v0.toNumeric(), f->object()->position().y() ),
			      f->interval()
			      );
  }
  else if( _name == "py" ) {
    
    f->object()->setPosition( Vector( f->object()->position().x(), v0.toNumeric() ),
			      f->interval()
			      );
  }
  else if( _name == "pd" ) {

    NOT_IMPLEMENTED( "pd" );
    // FIXME
  }


  else if( _name == "v" ) {
    
    f->object()->setVelocity( Vector( v0.toNumeric(),
				      v1.toNumeric() ) );
  }
  else if( _name == "vx" ) {

    f->object()->setVelocity( Vector( v0.toNumeric(), 
				      f->object()->velocity().y() ) );
  }
  else if( _name == "vy" ) {
    
    f->object()->setVelocity( Vector( f->object()->velocity().x(), 
				      v0.toNumeric() ) );
  }
  else if( _name == "vd" ) {
    
    // FIXME
    NOT_IMPLEMENTED( "vd" );
  }


  else if( _name == "a" ) {
    
    f->object()->setAcceleration( Vector( v0.toNumeric(),
					  v1.toNumeric() ) );
  }
  else if( _name == "ax" ) {

    f->object()->setAcceleration( Vector( v0.toNumeric(),
					  f->object()->acceleration().y() ) );
  }
  else if( _name == "ay" ) {
    
    f->object()->setAcceleration( Vector( f->object()->acceleration().x(), 
					  v0.toNumeric() ) );
  }
  else if( _name == "ad" ) {
    
    // FIXME
    NOT_IMPLEMENTED( "ad" );
  }
  else if( _name == "i" ) {

    f->setInterval( v0.toNumeric() );
  }
  else if( _name == "gp" ) {

    f->object()->setGravity( v0.toNumeric(), v1.toNumeric(), f->interval() );
  }
  else if( _name == "ht" ) {

    f->object()->setTarget( Object::TargetPlayer );
  }
  else if( _name == "htx" ) {

    f->object()->setTarget( Object::TargetFixed, f->program()->playerPosition() );
  }
  else {

    NOT_IMPLEMENTED( _name.c_str() );
  }
  

  return next();
}
