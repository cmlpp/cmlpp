#include <stdio.h>

#include "usercmdstatement.h"

#include "expression.h"
#include "expressionlist.h"
#include "fiber.h"
#include "trace.h"
#include "value.h"

UserCmdStatement::UserCmdStatement( const std::string& name, ExpressionList* arguments )
  : Statement( Statement::UserCmd ), _name( name ), _arguments( arguments )
{
}

UserCmdStatement::~UserCmdStatement()
{
  delete _arguments;
}

void UserCmdStatement::enter( Fiber* f )
{
}

Statement* UserCmdStatement::update( Fiber* f )
{
  TRACE;
  fprintf( stderr, "Executing user cmd: %s\n", _name.c_str() );
  fprintf( stderr, "  " );
  for( unsigned int i = 0; i < _arguments->numExpressions(); i++ ) {
    
    Expression* e = _arguments->expression( i );
    Value v = e->evaluate( f );
    
    std::string s = "??? ";
    switch( v.type() ) {

    case Value::Bool: 
      s = v.toBool() ? "true" : "false";
      break;

    case Value::Int: 
      {
	char buf[16];
	snprintf( buf, sizeof( buf ), "%d", v.toInt() );
	s = std::string( buf );
	break;
      }
    case Value::Float: 
      {
	char buf[16];
	snprintf( buf, sizeof( buf ), "%f", v.toNumeric() );
	s = std::string( buf );
	break;
      }
    case Value::String:
      s = v.toString();
      break;

    default:
      s = "NULL";
      break;
    }
    
    fprintf( stderr, "%s ", s.c_str() );
  }
  fprintf( stderr, "\n" );

  return next();
}
