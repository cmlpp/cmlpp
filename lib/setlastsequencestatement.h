#ifndef __CMLPP_SETLASTSEQUENCESTATEMENT_H__
#define __CMLPP_SETLASTSEQUENCESTATEMENT_H__

#include <string>

#include "statement.h"

class ExpressionList;
class Fiber;
class Sequence;

//! \internal
//! \brief Represents a "set last sequence" statement in the interpreter;
class SetLastSequenceStatement : public Statement
{
 public:
  SetLastSequenceStatement( ExpressionList* arguments, Sequence* sequence );
  ~SetLastSequenceStatement();
  
  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  ExpressionList* _arguments;
  Sequence* _sequence;
};

#endif // __CMLPP_SETLASTSEQUENCESTATEMENT_H__
