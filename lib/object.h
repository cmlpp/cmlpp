#ifndef __CMLPP_OBJECT_H__
#define __CMLPP_OBJECT_H__

#include "vector.h"

//! \brief Basic object type.
class Object
{
 public:

  enum MotionType
  {
    ConstantMotion,
    AccelerationMotion,
    InterpolationMotion,
    BulletMLMotion,
    GravityMotion
  };

  enum TargetType {

    TargetNone,
    TargetFixed,
    TargetPlayer,
  };

  Object( Object* parent );
  virtual ~Object();

  void init();

  void setParent( Object* parent );
  Object* parent();

  const Vector& position() const;
  Vector absolutePosition() const;
  void setPosition( const Vector& value, int interval );

  const Vector& velocity() const;
  void setVelocity( const Vector& value );

  const Vector& acceleration() const;
  void setAcceleration( const Vector& value );

  void setGravity( double gravity, double friction, int interval );

  float x() const;
  float absoluteX() const;

  float y() const;
  float absoluteY() const;

  void update();

  void setAllocated( bool value );
  bool isAllocated() const;

  void setTarget( TargetType type, const Vector& position = Vector() );

 private:
  Object* _parent;

  Vector _position;
  Vector _velocity;
  Vector _acceleration;
  Vector _diffAcceleration;

  MotionType _motionType;

  TargetType _targetType;
  Vector _targetPosition;

  int _interval;

  bool _allocated;
};

#endif // __CMLPP_OBJECT_H__
