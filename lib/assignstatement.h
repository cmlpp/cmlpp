#ifndef __CMLPP_ASSIGNSTATEMENT_H__
#define __CMLPP_ASSIGNSTATEMENT_H__

#include <string>

#include "statement.h"

class Expression;
class Fiber;

//! \internal
//! \brief Represents an assign statement ("l") in the interpreter.
class AssignStatement : public Statement
{
 public:
  AssignStatement( const std::string& name, const std::string& op, Expression* expression );
  ~AssignStatement();

  void enter( Fiber* f );
  Statement* update( Fiber* f );

 private:

  std::string _name;
  std::string _op;
  Expression* _expression;
};

#endif // __CMLPP_ASSIGNSTATEMENT_H__
