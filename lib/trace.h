#ifndef __CMLPP_TRACE_H__
#define __CMLPP_TRACE_H__

#include <string>


//! \internal
//! \brief Prints funtion entry/exit info using scope (utility class for debugging).
class Tracer
{
public:
  Tracer( const std::string& file, int line, const std::string& function );
  ~Tracer();

private:

  static std::string _indent;

  std::string _file;
  int _line;
  std::string _function;
};

//! \brief Macro to create a tracer instance with source code location info.
#define TRACE Tracer _tracer( __FILE__, __LINE__, __FUNCTION__ )

#endif // __CMLPP_TRACE_H__
