#ifndef __FIBER_H__
#define __FIBER_H__

#include <string>
#include <vector>

#include "value.h"

class LoopStatement;
class Object;
class Program;
class Statement;
class StatementList;

//! \internal
//! \brief Represents a Fiber (microthread) in the interpreter;
class Fiber
{
 public:

  //! Helper struct to keep track of loop iterations
  struct LoopStackEntry
  {
    LoopStatement* loop;
    int count;
  };

  Fiber( Program* program, StatementList* statements );

  //! \brief Indicates if the fiber has finished executing.
  bool isDone() const;

  //! \brief Runs the fiber for 1 tick.
  //!
  //! The fiber is run until the next blocking statement is reached.
  void update();

  unsigned int lastWaitFrames();
  unsigned int currentWaitFrames();
  void setCurrentWaitFrames( unsigned int frames);
  void pushWaitFrames( unsigned int frames );
  unsigned int popWaitFrames();

  LoopStackEntry* currentLoop();
  void setCurrentLoop( LoopStatement* loop, int value );
  void pushLoop();
  void pushLoop( LoopStatement* loop, int value );
  void popLoop();

  bool loopStackIsTop();
  
  void pushReturnIp( Statement* statement );

  void setGlobal( const std::string& name, const Value& value );
  Value global( const std::string& name ) const;

  Program* program();

  Object* object();
  void setObject( Object* o );

  bool hasInterval() const;
  int interval() const;
  void setInterval( int value );

  void dump();

 private:

  Program* _program;

  StatementList* _statements;
  Statement* _ip;
  unsigned int _index;

  std::vector< Statement* > _stack;

  std::vector< unsigned int > _waitFrames;
  unsigned int _lastWaitFrames;

  std::vector< LoopStackEntry > _loopStack;
  int _loopStackPtr;

  bool _done;

  Object* _object;

  int _interval;
};

#endif //  __FIBER_H__
