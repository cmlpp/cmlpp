#include <cassert>

#include "label.h"
#include "labellist.h"

LabelList::LabelList()
{
}

LabelList::~LabelList()
{
  // FIXME
}

void LabelList::append( Label* label )
{
  assert( label );

  _labels.push_back( label );

  // FIXME
  //label->setParent( this );
}

void LabelList::append( LabelList* list )
{
  assert( list );

  while( list->_labels.size() ) {

    _labels.push_back( list->_labels.front() );
    list->_labels.erase( list->_labels.begin() );
  }
}

void LabelList::prependName( const std::string& name )
{
  for( std::vector< Label* >::iterator it = _labels.begin();
       it != _labels.end(); it++ ) {
    
    (*it)->setName( name + "." + (*it)->name() );
  }
}

unsigned int LabelList::numLabels() const
{
  return _labels.size();
}

Label* LabelList::label( unsigned int index )
{
  assert( index < _labels.size() );

  return _labels[ index ];
}
